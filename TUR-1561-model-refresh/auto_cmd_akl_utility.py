import shutil
import subprocess
import gspread
import yaml
import pandas as pd
import numpy as np

from google.auth import default as gadefault
from google.cloud import bigquery
from google.cloud import bigquery_storage
from google.cloud import storage
from datetime import datetime

import time
import os

from feature_store import FeatureStore
import create_excel_config as ec

gcp_cred = f"/home/jovyan/.config/gcloud/legacy_credentials/"
gcp_cred += f"{os.getenv('JUPYTERHUB_USER')}/adc.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = gcp_cred
# os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = '/home/jovyan/.config/gcloud/application_default_credentials.json'

# ================ #
# Folder Structure #
# ================ #
# GS Project: google_project (default: 'wx-bq-poc')
# Example:
# cmd_version = 'v01'
# akl_version = 'v01'
# gs://google_bucket/personal_folder/model_name/0_sql_partition -- Bigquery Table to Google Cloud Storage
# gs://google_bucket/personal_folder/model_name/1_cmd_inputs/v01/
# ............................................................../cmd_input_v01.parquet
# ............................................................../sources_v01.txt
# gs://google_bucket/personal_folder/model_name/2_cmd_outputs/v01/
# .............................................................../part-00000.parquet
# .............................................................../part-00001.parquet
# .............................................................../part-*****.parquet
# gs://google_bucket/personal_folder/model_name/3_akl_inputs/v01_v01/
# ................................................................../argo.yaml
# ................................................................../conf.yaml
# ................................................................../exce.xlsx
# gs://google_bucket/personal_folder/model_name/4_akl_outputs/v01_v01/
# .................................................................../akl_outputs
# gs://bucket/folder/model_name/0_sql_partition/
# gs://bucket/folder/model_name/1_cmd_inputs/data_version/
# gs://bucket/folder/model_name/2_cmd_outputs/data_version/
# gs://bucket/folder/model_name/3_akl_inputs/data_version_model_version/
# gs://bucket/folder/model_name/4_akl_outputs/data_version_model_version/

#----------------#
# CMD Parameters #
#----------------#
# cmd_input_path #
# cmd_sourc_outp #
# cmd_ouput_path #
#================#

#-----------------#
# AKL Parameters  #
#-----------------#
# cmd_ouput_path  #
# argo.yaml       # 
# config.yaml     #
# excel_base.xlsx #
#=================#

class AutoCmdAklWrapper:
    
    def __init__(self, project, bucket, folder, model, data_version, model_version, source_file_content):
    
        self.project = project
        self.bucket = bucket
        self.folder = folder
        self.model = model
        self.data_version = data_version
        self.model_version = model_version
        self.feature_selection = source_file_content
        self.source_file_df = pd.DataFrame(data={'source': source_file_content})
        
        self.source_path_local = f'{data_version}/source.txt'
        self.source_path_gstor = f'gs://{bucket}/{folder}/{model}/1_cmd_inputs/{data_version}/source.txt'
        self.base_parque_gstor = f'gs://{bucket}/{folder}/{model}/1_cmd_inputs/{data_version}/base_input.parquet'
        self.cmd_ouput_path = f'gs://{bucket}/{folder}/{model}/2_cmd_outputs/{data_version}/'
        
        # TODO
        # self.cmd_base_parquet_path = f'gs://{bucket}/{folder}/{model}/0_sql_partition/'
        
        self.argo_path_templ = './config_file_templates/argo_base.yaml'
        self.conf_path_templ = './config_file_templates/config_base.yaml'
        self.exce_path_templ = './config_file_templates/excel_base.xlsx'
        self.argo_path_local = f'{data_version}_{model_version}/argo.yaml'
        self.conf_path_local = f'{data_version}_{model_version}/config.yaml'
        self.exce_path_local = f'{data_version}_{model_version}/excel.xlsx'
        self.argo_path_gstor = f'gs://{bucket}/{folder}/{model}/3_akl_inputs/{data_version}_{model_version}/argo.yaml'
        self.conf_path_gstor = f'gs://{bucket}/{folder}/{model}/3_akl_inputs/{data_version}_{model_version}/config.yaml'
        self.exce_path_gstor = f'gs://{bucket}/{folder}/{model}/3_akl_inputs/{data_version}_{model_version}/excel.xlsx'
        self.source_file_df.to_csv(self.source_path_gstor, index=False, header=False)
   

    def run_sql(self, base_parquet_type, commands, preprocess_func):
        df = None
                
        # cmd input config, source.txt
        # os.makedirs(os.path.dirname(self.source_path_local), exist_ok=True)
        # self.source_file_df.to_csv(self.source_path_local, index=False, header=False)
        
        if base_parquet_type == 'sql':
            print('Step 1/3 Loading Data from BigQuery ....')
            bq = bigquery.Client(project=self.project)
            df = bq.query(commands).result().to_dataframe()
            print('Step 1/3 Loading Data from BigQuery Done')
            print('  Base CMD input parquet shape', df.shape)
        elif base_parquet_type == 'gcs':
            print('gcs is under TODO List')
            # TODO
            '''
            cmd_base_parquet_path_gstor = ''
            cmd_base_parquet_list = []
            while cmd_base_parquet_path_gstor != self.cmd_base_parquet_path or len(cmd_base_parquet_list)==0:
                cmd_base_parquet_path_gstor = input(f'Press Enter {self.cmd_base_parquet_path} to confirm)
                print('Loading BigQuery Exported Files')
            '''
        else:
            raise NameError(base_parquet_type + ' is not available. Please select from {slq, gcs}')

        if df is not None:
            print('Step 2/3 Preprocessing Base Parquet ....')
            df = preprocess_func(df)
            print('Step 2/3 Preprocessing Base Parquet Done')
        else:
            raise ValueError('CMD pivoter input base parquet is None')

        print('Step 3/3 Saving Data to GCP Storage ....')
        df.to_parquet(self.base_parque_gstor, index=False)
        print('Step 3/3 Saving Data to GCP Storage Done')
        
        
    def create_cmd_inputs(self, base_parquet_type, commands, preprocess_func):
        
        client = storage.Client(project=self.project)
        temp_base_parque_gstor = self.base_parque_gstor.replace(f'gs://{self.bucket}/', '')
        bucket = client.bucket(self.bucket)
        stats = storage.Blob(bucket=bucket, name=temp_base_parque_gstor).exists(client)
        
        if stats:
            print('Base_input_parquet Already Exist at ' + self.base_parque_gstor)
            arg = input('Do you want to skip? Y --skip, N --continue')
            print(f'  {arg}')
            
            if arg == 'Y':
                print('Using Existing Base_input_parquet')
            elif arg == 'N':
                self.run_sql(base_parquet_type, commands, preprocess_func)
        else:        
            self.run_sql(base_parquet_type, commands, preprocess_func)   
    
    
    def run_cmd(self):
        input__path = self.base_parque_gstor
        output_path = self.cmd_ouput_path
        source_path = self.source_path_gstor
        bash_string = 'gcloud --project wx-bq-poc dataproc workflow-templates instantiate preprod-spark-cmd2-pivoter-v2 --async --region us-east4 --parameters \\\n'
        bash_string += f'INPUT_PATH={input__path},\\\n'
        bash_string += f'INPUT_TYPE=parquet,\\\n'
        bash_string += f'OUTPUT_PATH={output_path},\\\n'
        bash_string += f'OUTPUT_TYPE=parquet,\\\n'
        bash_string += f'CSV_DELIMITER=\'|\',\\\n'
        bash_string += f'PARTITION_FORMAT=yyyy-MM-dd,\\\n'
        bash_string += f'JOIN_TYPE=left,\\\n'
        bash_string += f'DECIMAL_AS_DOUBLE=true,\\\n'
        bash_string += f'FILE_INCLUDED_SOURCES={source_path},\\\n'
        bash_string += f'FILE_INCLUDED_FEATURES=na,\\\n'
        bash_string += f'FILE_FEATURE_FILTER=na,\\\n'
        bash_string += f'INCLUDE_FUEL=false,\\\n'
        bash_string += f'NO_SUBFOLDER=true,\\\n'
        bash_string += f'OUT_FILE_NUM=50,\\\n'
        bash_string += f'SPARSITY_UPLIMIT=1'
        self.excute_bash(bash_string)

    def copy_blob(
        bucket_name, blob_name, destination_bucket_name, destination_blob_name
    ):
        """Copies a blob from one bucket to another with a new name."""
        # bucket_name = "your-bucket-name"
        # blob_name = "your-object-name"
        # destination_bucket_name = "destination-bucket-name"
        # destination_blob_name = "destination-object-name"

        storage_client = storage.Client()

        source_bucket = storage_client.bucket(bucket_name)
        source_blob = source_bucket.blob(blob_name)
        destination_bucket = storage_client.bucket(destination_bucket_name)

        blob_copy = source_bucket.copy_blob(
            source_blob, destination_bucket, destination_blob_name
        )

        print(
            "Blob {} in bucket {} copied to blob {} in bucket {}.".format(
                source_blob.name,
                source_bucket.name,
                blob_copy.name,
                destination_bucket.name,
            )
        )    
    def run_cmd3(self):
        input_path = self.base_parque_gstor
        output_path = self.cmd_ouput_path
        feature_selection = self.feature_selection

        print('Loading entity df ...')
        df_entity = pd.read_parquet(self.base_parque_gstor)
        
        #=====================#
        # Set up FeatureStorm #
        #=====================#
        fs = FeatureStore(env='prod')
        fs_param_dict = {
            'keys': ['crn'],
            'feature_selection': feature_selection,
            'entity_path': df_entity,
            'output_type': 'parquet',
            'out_file_num': 50,
            # only sparsity < sparsity_uplimit will be picked
            'sparsity_uplimit': 1.0,
            'convert_numeric_to_double': True
        }
        print('Running feature store ...')
        output_path_fs = fs.batch.fetch_features_to_gcs(**fs_param_dict)
        # print(f'gsutil -m cp -r {output_path_fs} {output_path}')
        self.excute_bash(f'gsutil -m cp -r {output_path_fs}* {output_path}')
        
    def excute_cmd(self):
        
        cmd_partition_list = []
        client = storage.Client(project=self.project)
        for blob in client.list_blobs(self.bucket, prefix=self.cmd_ouput_path.replace(f'gs://{self.bucket}/', '')):
            if '.parquet' in blob.name:
                cmd_partition_list.append(blob.name)
                
                
        if len(cmd_partition_list) == 50:
            print('CMD Pivoter Already Finished at ' + self.cmd_ouput_path)
            arg = input('Do you want to skip CMD Pivoter? Y --skip, N --continue')
            print(f'  {arg}')
            
            if arg == 'Y':
                print('Using Existing CMD Pivoter Result')
                
            elif arg == 'N':
                self.run_cmd3()
        else:
            self.run_cmd3()
    
    
    def detect_end_of_cmd(self, sleep_interval):
        
        # TODO max_waiting
        # ======================================= #
        # retrieve cmd output partition name list #
        # ======================================= #
        self.cmd_partition_list = []
        while len(self.cmd_partition_list) != 50:
            self.cmd_partition_list = []
            print(f'Checking CMD Pivoter @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
            client = storage.Client(project=self.project)
            for blob in client.list_blobs(self.bucket, prefix=f'{self.folder}/{self.model}/2_cmd_outputs/{self.data_version}/'):
                if '.parquet' in blob.name:
                    self.cmd_partition_list.append(blob.name)

            if len(self.cmd_partition_list) == 50:
                print(f'CMD Pivoter Finished @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
                break
                
            time.sleep(60 * sleep_interval)
        
    
    def generate_akl_config_files(self):
    
        with open(self.argo_path_templ) as file:
            argo_dict = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
        with open(self.conf_path_templ) as file:
            conf_dict = yaml.load(file, Loader=yaml.FullLoader)
        file.close()
    
        argo_dict['spec']['arguments']['parameters'][0]['value'] = self.conf_path_gstor
        argo_dict['metadata']['generateName'] = f'{self.folder}-{self.model}'.replace('_','-').lower()
        conf_dict['global']['google_project'] = self.project
        conf_dict['global']['google_bucket'] = self.bucket
        conf_dict['global']['google_subdir'] = f'{self.folder}/{self.model}/4_akl_outputs/{self.data_version}_{self.model_version}'
        conf_dict['global']['model_name'] = self.model
        conf_dict['input']['data_addr'] = self.cmd_ouput_path.replace('gs://','') + '*'
        conf_dict['input']['config_addr'] = self.exce_path_gstor.replace('gs://','')
        
        print('Loading CMD Output Parquet ....')
        cmd_output_parquet_gs_path = f'gs://{self.bucket}/{self.cmd_partition_list[0]}'
        print(cmd_output_parquet_gs_path)
        cmd_output_parquet_df = pd.read_parquet(cmd_output_parquet_gs_path)
        print('Loading CMD Output Parquet Done')
        
        cat_threshold = 50
        exce_dict = ec.generate_excel_config(cmd_output_parquet_df, cat_threshold)
        
        return argo_dict, conf_dict, exce_dict
    
    
    def create_akl_inputs(self, argo_dict, conf_dict, exce_dict):
        
        os.makedirs(os.path.dirname(self.argo_path_local), exist_ok=True)
        
        # config.yaml validation
        # validate preprocessing params
        rate = conf_dict['preprocessor']['params']['train_test_holdout']['by_proportion']
        if isinstance(rate, float) or isinstance(rate, int):
            pass
        elif isinstance(rate, list):
            pass
        else:
            raise TypeError("config.yaml ['preprocessor']['params']['train_test_holdout']['by_proportion'] has to be a number or a list")
            
        with open(self.argo_path_local, 'w') as temp_local_file:
            yaml.dump(argo_dict, temp_local_file, default_flow_style=False)
        temp_local_file.close()
        
        with open(self.conf_path_local, 'w') as temp_local_file:
            yaml.dump(conf_dict, temp_local_file, default_flow_style=False)
        temp_local_file.close()
        
        writer = pd.ExcelWriter(self.exce_path_local, engine='xlsxwriter')
        exce_dict['tables'].to_excel(writer, sheet_name='tables', index=False)
        exce_dict['mars_data_dictionary'].to_excel(writer, sheet_name='mars_data_dictionary', index=False)
        exce_dict['feature_spec_num'].to_excel(writer, sheet_name='feature_spec_num', index=False)
        exce_dict['feature_spec_cat'].to_excel(writer, sheet_name='feature_spec_cat', index=False)
        exce_dict['Constrain'].to_excel(writer, sheet_name='Constrain', index=False)
        writer.save()
        
        client = storage.Client(project=self.project)
        bucket = client.get_bucket(self.bucket)
        blob = bucket.blob(self.argo_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.argo_path_local)
        
        blob = bucket.blob(self.conf_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.conf_path_local)
        
        blob = bucket.blob(self.exce_path_gstor.replace(f'gs://{self.bucket}/',''))
        blob.upload_from_filename(filename=self.exce_path_local)   
        
        
    def excute_akl(self):
        bash_string = 'gcloud --project wx-bq-poc container clusters get-credentials project-melon --zone=us-west1-a\n'
        bash_string += f'sudo curl -SSL -o /usr/local/bin/argo https://github.com/argoproj/argo-workflows/releases/download/v2.2.0/argo-linux-amd64\n'
        bash_string += f'sudo chmod +x /usr/local/bin/argo\n'
        bash_string += f'argo submit {self.argo_path_local}'
        self.excute_bash(bash_string)
    
    
    def excute_bash(self, bash_string):
        print('='*50)
        print('bash input command:'.center(50))
        print('-'*50)
        print(bash_string)
        print('='*50)       
        shell_output = subprocess.check_output(bash_string, stderr=subprocess.STDOUT, shell=True, executable='/bin/bash')
        print('bash output results:'.center(50))
        print('-'*50)
        print(shell_output.decode("utf-8") )
        print('='*50)