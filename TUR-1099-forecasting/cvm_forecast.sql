DECLARE latest_date DATE;
SET latest_date = (
    SELECT MAX(offer_start_date) 
    FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view` fcs
    WHERE campaign_code IN (
        SELECT DISTINCT campaign_code
        FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM.MMM_AUDIENCE_MASTER`
    )
);

WITH dates_campaign AS (
    SELECT * 
    FROM (
        SELECT DISTINCT fw_start_date
        FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
        WHERE fw_start_date >= '2020-08-31'
            AND fw_start_date <= latest_date
    )
        CROSS JOIN (
            SELECT DISTINCT 
                campaign_code, 
                CASE WHEN campaign_type = 'MP' AND campaign_length = 7 THEN 'MP1'
                        WHEN campaign_type = 'MP' AND campaign_length = 21 THEN 'MP3'
                        WHEN campaign_type = 'SS' AND campaign_length = 7 THEN 'SS1'
                        WHEN campaign_type = 'SS' AND campaign_length = 14 THEN 'SS2'
                        ELSE campaign_type 
                END AS offer_type,
            FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM.MMM_AUDIENCE_MASTER`
        )
    WHERE offer_type NOT IN ('OSS','MP3')
    GROUP BY 1, 2, 3
),

master_audience AS (
    SELECT
        campaign_start_date,
        campaign_code,
        COUNT(DISTINCT crn) AS audience
    FROM
        `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view`
    WHERE campaign_code IN (
        SELECT DISTINCT campaign_code 
        FROM`gcp-wow-rwds-ai-mmm-prod.PROD_MMM.MMM_AUDIENCE_MASTER`
    )
    GROUP BY 1, 2
),

promo AS (
    SELECT 
        fcs.campaign_start_date,
        fcs.offer_start_date,
        fcs.campaign_week_nbr,
        a.campaign_code,
        a.audience,
        0 AS post_week_flag,
        COUNT(DISTINCT fcs.redeem_flag) AS redeemers,
        SUM(fcs.attributed_inc_sales) AS inc_sales,
        SUM(fcs.reward_val) AS reward_val
    FROM
        `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view` fcs
        JOIN master_audience a 
            ON fcs.campaign_start_date = DATE(a.campaign_start_date)
            AND fcs.campaign_code = a.campaign_code
    GROUP BY 1, 2, 3, 4, 5
), 

post AS (
    SELECT  
        ca.campaign_start_date,
        ca.fw_start_date,
        DATE_DIFF(ca.fw_start_date, ca.campaign_start_date, DAY) / 7 + 1 AS campaign_week_nbr,
        ca.campaign_code,
        a.audience,
        1 AS post_week_flag,
        0 AS redeemers,
        SUM(ca.inc_sales) AS inc_sales,
        0 AS reward_val
    FROM 
        `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.cp_att_crn_halo_post_all_view` ca
        JOIN master_audience a 
            ON ca.campaign_start_date = DATE(a.campaign_start_date)
            AND ca.campaign_code = a.campaign_code
    GROUP BY 1, 2, 3, 4, 5
),

promo_post AS (
    SELECT * FROM promo 
    UNION ALL
    SELECT * FROM post
)

SELECT
    dc.offer_type,
    dc.fw_start_date AS ds,
    campaign_week_nbr,
    SUM(audience) AS audience,
    SUM(redeemers) AS redeemers,
    SUM(inc_sales) AS inc_sales,
    SUM(reward_val) AS cost
FROM 
    dates_campaign dc
    LEFT JOIN promo_post pp
        ON dc.fw_start_date = pp.offer_start_date
        AND dc.campaign_code = pp.campaign_code
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3;