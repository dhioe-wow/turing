import os
import datetime

from matplotlib import pyplot as plt
import numpy as np
import pandas as pd

from google.cloud import bigquery
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 

def train_test_split(df, forecast_df):
    train_df = df[df.ds < forecast_df.ds.min()]
    test_df = df[df.ds.isin(list(forecast_df.ds.unique()))]

    return train_df, test_df


def manage_overlap(df_1, df_2):
    df_1 = df_1.merge(df_2, how='left', left_on='ds', right_on='ds')
    df_1 = df_1.fillna(0)
    df_1['audience'] = df_1['audience_x'] + df_1['audience_y']
    df_1['inc_sales'] = df_1['inc_sales_x'] + df_1['inc_sales_y']
    df_1['cost'] = df_1['cost_x'] + df_1['cost_y']
    df_1['redeemers'] = df_1['redeemers_x'] + df_1['redeemers_y']

    return df_1[['ds', 'audience', 'inc_sales', 'cost', 'redeemers']]


def train_test_plot(train_df, test_df, forecast_df, period):
    assert (period == 'weekly') or (period == 'monthly')

    METRICS = ['inc_sales', 'cost', 'redeemers']
    for metric in METRICS:
        plt.figure(figsize=(15,3))
        plt.title(f'{metric}')
        plt.plot(train_df.ds, train_df[metric], color='k', label='train')
        if period == 'weekly':
            plt.plot(test_df.ds, test_df[metric], color='r', label='test')
            plt.plot(forecast_df.ds, forecast_df[metric], color='b', label='forecast')
        elif period == 'monthly':
            plt.plot(test_df.ds, test_df[metric], '.r', label='test')
            plt.plot(forecast_df.ds, forecast_df[metric], '.b', label='forecast')
        plt.grid()
        plt.legend()
        plt.show()

        try:
            sMAPE = eval_smape(forecast_df[metric], test_df[metric])
        except:
            sMAPE = 'Not enough data to compute'

        print(f'sMAPE = {sMAPE}')


def get_weekly(df):
    df = df.groupby(['ds']).aggregate({'audience': 'sum', 'inc_sales': 'sum', 'cost': 'sum', 'redeemers':'sum'}).reset_index()
    return df


def get_monthly(df, fp_start_date):
    sql = f'''
        SELECT * 
        FROM (
            SELECT DISTINCT 
                fp_start_date AS dt, 
                fw_start_date AS ds
            FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
            WHERE fp_start_date >= '2020-08-31'
                AND DATE(fp_start_date) <= '{fp_start_date}'
        )
    '''
    bq = bigquery.Client(project='gcp-wow-rwds-ai-mmm-prod')
    monthly = bq.query(sql).result().to_dataframe()

    df = monthly.merge(df, left_on='ds', right_on='ds').sort_values(by='dt')
    df = df.groupby('dt').aggregate({
        'audience': 'sum',
        'inc_sales': 'sum',
        'cost': 'sum',
        'redeemers': 'sum',
    }).reset_index()
    df.rename(columns={'dt': 'ds'}, inplace=True)
    return df


def eval_smape(y, yhat):
    y = np.array(y)
    yhat = np.array(yhat)
    return np.mean(np.abs(yhat - y) / (np.abs(yhat) + np.abs(y))) * 100