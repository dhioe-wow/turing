import os
import copy
import datetime, time

import numpy as np
import pandas as pd

import util

from google.cloud import bigquery
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 


class MMMForecast:
    '''Main class for producing forecast based on simple exponential extrapolation, done on campaign-execution-week level.
    Example:

    ------------|---------------|-------------------|-----------       
    offer_type  | ds            | campaign_week_nbr | rpc    
    ------------|---------------|-------------------|-----------
    2C          | 2021-09-27    | 1                 | 2.06
    2C          | 2021-09-13    | 1                 | 2.63
    2C          | 2021-08-30    | 1                 | 1.35
    ....        | ...           | ...               | ...


    Choosing COEFF as 0.5, we have prediction:
    ------------|-------------------|-----------       
    offer_type  | campaign_week_nbr | rpc    
    ------------|-------------------|-----------
    2C          | 1                 | (2.06 *0.5^0) + (2.63 * 0.5^1) + (1.35 * 0.5^2) + ...) / (0.5^0 + 0.5^1 + 0.5^2 + ...)
    ....        | ...               | ...

    * The power is determined from time gap (in weeks/fortnights).

    The above extrapolation is done for each metric (rpc, rdm, cpc, and audience proportion and size).
    The campaign is then simulated to the future (6 iterations at default), using the above statistics, and aggregated 
    to finally yield the forecast of metric by financial weekly/monthly.

    '''

    def __init__(self):
        '''Initialise by taking sql_filename from Child object'''
        self.bq = bigquery.Client(project='gcp-wow-rwds-ai-mmm-prod')
        self.sql_filename = None
        self.bq_tablename = None
        self.exclusion_dates = []


    def load_data(self):
        '''Loading data from sql script.
        We only accept sql query that will yield the following columns:
        - offer_type
        - ds
        - campaign_week_nbr
        - audience
        - redeemers
        - inc_sales
        - cost
        where `ds` is the start of financial week. For the script to run properly, 
        a cartesian product of offer_type and ds is expected.
        '''
        with open(self.sql_filename, 'r') as f:
            sql = [line for line in f]
        sql = ''.join(sql)

        return self.bq.query(sql).result().to_dataframe()


    def preprocess_data(self, df):
        '''Preprocess data
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest

        return: [pd.DataFrame] with rates value (per crn)
        '''
        df['inc_sales'] = df['inc_sales'].astype(float)
        df['redeemers'] = df['redeemers'].astype(float)
        df['cost'] = df['cost'].astype(float)
        df['audience'] = df['audience'].astype(float)

        df['rpc'] = df['inc_sales'] / df['audience']
        df['rdm'] = df['redeemers'] / df['audience']
        df['cpc'] = df['cost'] / df['audience']

        return df


    def get_audience(self, df, coeff=1.0):
        '''Get audience statistics
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest
            - coeff [float]: exponential decay coefficient

        return: 
            - [dict] containing the expected proportion of audience for each offer_type
            - [float] representing the expected total number of audience
        '''

        OFFER_TYPES = list(df['offer_type'].unique())
        df_audience = df[df['campaign_week_nbr'] == 1].groupby('ds').aggregate({'audience':'sum'}).reset_index()

        for offer_type in OFFER_TYPES:
            df_audience_offer = df[(df['campaign_week_nbr'] == 1) & (df['offer_type'] == offer_type)][['ds', 'audience']]
            df_audience = df_audience.merge(df_audience_offer, how='left', left_on='ds', right_on='ds')
            df_audience['audience_y'] = df_audience['audience_y'] / df_audience['audience_x']
            df_audience.rename(columns={'audience_x': 'audience', 'audience_y': f'audience_{offer_type}'}, inplace=True)
            
        last_date = df_audience['ds'].max()
        df_audience['delta'] = df_audience['ds'].apply(lambda x: (last_date-x).days / 14.0)
        df_audience['w'] = df_audience['delta'].apply(lambda x: coeff ** x)
        df_audience['w'] = df_audience.apply(lambda row: self.update_weight(row), axis=1)

        expected_total_audience = np.sum(df_audience['w'] * df_audience['audience']) / np.sum(df_audience['w'])
        expected_audience = {offer_type: 0 for offer_type in OFFER_TYPES}

        for offer_type in OFFER_TYPES:
            num = np.sum(df_audience['w'] * df_audience[f'audience_{offer_type}'])
            den = np.sum(df_audience['w'] * df_audience[f'audience_{offer_type}'].apply(lambda x: 0 if np.isnan(x) else 1))
            expected_audience[offer_type] = num / den

        for offer_type in OFFER_TYPES:
            expected_audience[offer_type] = expected_audience[offer_type] / np.sum(list(expected_audience.values()))

        return expected_audience, expected_total_audience

    
    def get_weight(self, df, coeff=1.0): 
        '''Get weighted future value for rates metric
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest
            - coeff [float]: exponential decay coefficient

        return: 
            - [pd.DataFrame] by offer_type and campaign_week_nbr, the weighted rate metrics
        '''

        def _get_weight(row, coeff):
            weight = (row['ds_y']-row['ds_x']).days / 14.0
            weight = coeff ** weight
            return weight

        _df = df.groupby(['offer_type', 'campaign_week_nbr']).aggregate({'ds':'max'}).reset_index()
        _df = df.merge(_df, left_on=['offer_type', 'campaign_week_nbr'], right_on=['offer_type', 'campaign_week_nbr'])

        _df['w'] = _df.apply(lambda row: _get_weight(row, coeff), axis=1)
        _df.rename(columns={'ds_x': 'ds'}, inplace=True)
        _df.drop(columns=['ds_y'], inplace=True)
        _df['w'] = _df.apply(lambda row: self.update_weight(row), axis=1)

        _df['wrpc'] = _df['rpc'] * _df['w']
        _df['wrdm'] = _df['rdm'] * _df['w']
        _df['wcpc'] = _df['cpc'] * _df['w']

        df_offer = _df.groupby(['offer_type', 'campaign_week_nbr']).aggregate({'w':'sum', 'wrpc':'sum', 'wrdm':'sum', 'wcpc':'sum'}).reset_index()
        df_offer['rpc'] = df_offer['wrpc'] / df_offer['w']
        df_offer['rdm'] = df_offer['wrdm'] / df_offer['w']
        df_offer['cpc'] = df_offer['wcpc'] / df_offer['w']

        df_offer = df_offer[['offer_type','campaign_week_nbr', 'rpc', 'rdm', 'cpc']]

        return df_offer


    def update_weight(self, row):
        '''Update weight if there are exclusion dates, i.e. evaluate to 0 so not to be counted.
        Example usage: df['w'] = df.apply(lambda row: self.update_weight(row), axis=1)
        args:
            - row [pd.Series]: row of pandas dataframe, containing at least date [ds] and weight [w]

        returns: 
            - [float] representing the updated weight 
        '''

        if row['ds'] in self.exclusion_dates:
            return 0
        else:
            return row['w']


    def simulate(self, df, window=6, coeff=1.0, offer_pool=None):
        '''Simulate the offer allocations
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest
            - window [int]: the window of prediction
            - coeff [float]: exponential decay coefficient
            - offer_pool [dict]: dictionary containing execution dates as key and list of exlcuded campaign as values

        returns:
            - [pd.DataFrames] containing the forecast values of audience size
        '''

        OFFER_TYPES = list(df['offer_type'].unique())
        OFFER_LENGTH = df.groupby(['offer_type']).aggregate({'campaign_week_nbr': 'max'}).dropna().to_dict()['campaign_week_nbr']

        OFFER_DATES = [ df.ds.max() + datetime.timedelta(days=7*i) for i in range(1,window+1) ]
        OFFER_POOL = {
            offer_date: [] for offer_date in OFFER_DATES
        }

        if offer_pool:
            for offer_date in OFFER_DATES:
                if offer_date in offer_pool.keys():
                    OFFER_POOL[offer_date] = offer_pool[offer_date]
        
        EXPECTED_AUDIENCE, EXPECTED_TOTAL_AUDIENCE = self.get_audience(df, coeff)
    
        def _simulate(df, next_date, offer_type, audience):
            offer_length = OFFER_LENGTH[offer_type]  
            offer_audience = audience[offer_type]

            last_exec_date = df[df.campaign_week_nbr == 1].ds.max()
            last_date = df.ds.max()

            next_df = df[df['offer_type'] == offer_type][['offer_type', 'ds', 'campaign_week_nbr', 'audience']]
            next_df = next_df[next_df['ds'] == last_date]
            next_df['ds'] = next_date
            next_df['campaign_week_nbr'] += 1
            next_df = next_df[next_df['campaign_week_nbr'] <= offer_length]

            if last_exec_date == last_date:
                next_df.reset_index(drop=True, inplace=True)
            else:
                next_df=next_df.append(
                    pd.DataFrame({
                        'offer_type': [offer_type],
                        'ds': [next_date],
                        'campaign_week_nbr': [1.0],
                        'audience': [offer_audience],
                    }), ignore_index=True
                )            

            return next_df

        results = []
        for offer_type in OFFER_TYPES:
            
            next_df = df.copy()
            for next_date in OFFER_DATES:
            
                # scaling audience size if there are exclusions
                scaled_audience = copy.deepcopy(EXPECTED_AUDIENCE)
                
                total_scaled_audience = 0
                for _offer_type in OFFER_TYPES:
                    if _offer_type in OFFER_POOL[next_date]:
                        scaled_audience[_offer_type] = 0
                    total_scaled_audience += scaled_audience[_offer_type]

                for _offer_type in OFFER_TYPES:     
                    scaled_audience[_offer_type] = scaled_audience[_offer_type] / total_scaled_audience * EXPECTED_TOTAL_AUDIENCE
            
                next_df = _simulate(next_df, next_date, offer_type, scaled_audience)
                results.append(next_df)

        results = pd.concat(results, ignore_index=True)

        return results

    
    def truncate(self, df, test_date=None, train_date=None):
        '''Automatically truncate df compute required window for forecasting for a financial month
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest
            - test_date [str]: string representing date to be predicted, financial month start: yyyy-mm-dd
            - train_date [str]: string representing the latest date of data is available for training, financial week start: yyyy-mm-dd
        returns:
            - [pd.DataFrame] representing the truncated window
            - [int] representing the forecast window
            - [date] representing the last date to be predicted, financial week start
        '''

        if test_date:
            sql = f'''
                SELECT MAX(fw_start_date)
                FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
                WHERE fp_start_date = '{test_date}';
            '''
        else:
            sql = f'''
                SELECT MAX(fw_start_date)
                FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
                WHERE fp_start_date = (
                    SELECT MIN(fp_start_date) AS fp_start_date
                    FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
                    WHERE fp_start_date > CURRENT_DATE('Australia/Sydney')
                );
            '''
        pred_date = list(self.bq.query(sql).result())[0][0]
        
        if train_date:
            train_date = datetime.datetime.strptime(train_date, '%Y-%m-%d').date()
        else:
            train_date = df.ds.max()

        window = (pred_date - train_date).days / 7
        
        cut_date = pred_date - datetime.timedelta(window*7)
        df = df[df.ds <= cut_date] # cut data to allow for window

        window = int(window)
        return df, window, pred_date


    def forecast(self, df, window=6, coeff=1.0, offer_pool=None):
        '''Putting simulation results of offer allocation and expected rates together to evaluate forecast
        args:
            - df [pd.DataFrame]: dataframe containing the time series of interest
            - window [int]: the window of prediction
            - coeff [float]: exponential decay coefficient
            - offer_pool [dict]: dictionary containing execution dates as key and list of exlcuded campaign as values
        returns:
            - [pd.DataFrames] containing the forecast values
        '''

        df_offer = self.get_weight(df, coeff)
        df_sim = self.simulate(df, window, coeff, offer_pool)

        df_forecast = df_sim.merge(df_offer, left_on=['offer_type','campaign_week_nbr'], right_on=['offer_type','campaign_week_nbr'])
        df_forecast = df_forecast.sort_values(by=['offer_type', 'ds', 'campaign_week_nbr'])

        df_forecast['inc_sales'] = df_forecast['rpc'] * df_forecast['audience']
        df_forecast['cost'] = df_forecast['cpc'] * df_forecast['audience']
        df_forecast['redeemers'] = df_forecast['rdm'] * df_forecast['audience']

        return df_forecast


    def run(self, coeff, offer_pool, test_date=None, train_date=None):
        '''Overarching method that loads data, preprocess, and perform forecast
        args:
            - coeff [float]: exponential decay coefficient
            - offer_pool [dict]: dictionary containing execution dates as key and list of exlcuded campaign as values
            - test_date [str]: string representing date to be predicted, financial month start: yyyy-mm-dd
            - train_date [str]: string representing the latest date of data is available for training, financial week start: yyyy-mm-dd
        returns:
            - [pd.DataFrames] containing the forecast values, aggregated weekly and monthly
        '''

        print(50*"=")
        print(f'Forecasting for financial month: {test_date}.')
        print(50*"=")

        time_0 = time.time()

        print("Loading data...")
        df = self.load_data()
        print("Preprocess data...")
        df = self.preprocess_data(df)

        print("Forecasting...")
        df, window, pred_date = self.truncate(df, test_date=test_date, train_date=train_date)
        forecast_df = self.forecast(df, window=window, coeff=coeff, offer_pool=offer_pool)

        print("Summarising to weekly and monthly views...")
        # aggregate to weekly and monthly
        weekly_df = util.get_weekly(df)
        weekly_df['data_type'] = 'history'
        weekly_forecast_df = util.get_weekly(forecast_df)
        weekly_forecast_df['data_type'] = 'forecast' 
        weekly_df = pd.concat([weekly_df, weekly_forecast_df], ignore_index=True)
        weekly_df['insert_ts'] = datetime.datetime.today().date()

        monthly_df = util.get_monthly(df, pred_date)
        monthly_df['data_type'] = 'history'
        monthly_forecast_df = util.get_monthly(forecast_df, pred_date)
        monthly_forecast_df['data_type'] = 'forecast' 

        # need to deal with overlap for monthly forecast
        monthly_df = monthly_df.merge(monthly_forecast_df, how='outer', left_on='ds', right_on='ds')
        monthly_df['data_type'] = monthly_df['data_type_y'].fillna('history')
        monthly_df = monthly_df.fillna(0)
        monthly_df['audience'] = monthly_df['audience_x'] + monthly_df['audience_y']
        monthly_df['inc_sales'] = monthly_df['inc_sales_x'] + monthly_df['inc_sales_y']
        monthly_df['cost'] = monthly_df['cost_x'] + monthly_df['cost_y']
        monthly_df['redeemers'] = monthly_df['redeemers_x'] + monthly_df['redeemers_y']

        monthly_df = monthly_df[['ds', 'audience', 'inc_sales', 'cost', 'redeemers', 'data_type']]
        monthly_df['insert_ts'] = datetime.datetime.today().date()

        print(f"Uploading to BigQuery: `dhioe.MMM_{self.bq_tablename}_WEEK/MONTH`...")
        weekly_df['ds'] = weekly_df['ds'].astype('datetime64[ms]')
        monthly_df['ds'] = monthly_df['ds'].astype('datetime64[ms]')
        weekly_df['insert_ts'] = weekly_df['insert_ts'].astype('datetime64[ms]')
        monthly_df['insert_ts'] = monthly_df['insert_ts'].astype('datetime64[ms]')

        # upload to big query
        weekly_df.to_gbq(
            f'dhioe.MMM_{self.bq_tablename}_WEEK', 
            project_id='gcp-wow-rwds-ai-mmm-prod', 
            progress_bar=False,
            if_exists='append')

        monthly_df.to_gbq(
            f'dhioe.MMM_{self.bq_tablename}_MONTH', 
            project_id='gcp-wow-rwds-ai-mmm-prod', 
            progress_bar=False,
            if_exists='append')

        print('Done.')
        print(f'Time taken: {time.time()-time_0:.1f} sec.')
        print(50*"-")

        return weekly_df, monthly_df


class CVMForecast(MMMForecast):
    '''The child class of MMMForecast that specialises in CVM'''

    def __init__(self):
        super().__init__()
        self.sql_filename = 'cvm_forecast.sql'
        self.bq_tablename = 'CVM_FORECAST'
        self.exclusion_dates = []


class CATForecast(MMMForecast):
    '''The child class of MMMForecast that specialises in CAT'''

    def __init__(self):
        super().__init__()
        self.sql_filename = 'cat_forecast.sql'
        self.bq_tablename = 'CAT_FORECAST'
        self.exclusion_dates = []


    def load_data(self):

        # Check the gaps in cost data
        sql = '''
            SELECT MAX(offer_start_date) 
            FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_solus_view` fcs
            WHERE campaign_code IN (
                SELECT DISTINCT campaign_code || '-' || campaign_type
                FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
            ) 
        '''

        fcs_latest_date = list(self.bq.query(sql).result())[0][0]

        sql = '''
            SELECT MAX(fw_start_date) 
            FROM `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FORECAST_COST` fcs
        '''

        ext_latest_date = list(self.bq.query(sql).result())[0][0]

        fcs_latest_date_str = fcs_latest_date.strftime('%Y-%m-%d')
        ext_latest_date_str = ext_latest_date.strftime('%Y-%m-%d')


        sql = f'''
            SELECT DISTINCT fw_start_date 
            FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date` fcs
            WHERE fw_start_date > '{ext_latest_date_str}' 
                AND fw_start_date <= '{fcs_latest_date_str}'
        '''

        update_dates = self.bq.query(sql).result().to_dataframe()
        update_dates = [update_date.strftime('%Y-%m-%d') for update_date in list(update_dates['fw_start_date'])]

        if len(update_dates) > 0:
            print(f'Cost table needs updating for dates: {update_dates}')
            print('Updating cost table...')

            # Update cost data
            for update_date in update_dates:
                sql = f'''
                    CALL `gcp-wow-rwds-ai-mmm-prod.dhioe.UPDATE_FORECASTING_COST` (
                        '{update_date}'
                    )
                '''
                response = self.bq.query(sql).result()

            print('Finished cost table update. Proceed to forecast...')

        df = super().load_data()
        return df
    
if __name__ == '__main__':

    forecast = MMMForecast()
    forecast.run()

