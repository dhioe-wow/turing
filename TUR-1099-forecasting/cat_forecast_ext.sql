CREATE OR REPLACE PROCEDURE `gcp-wow-rwds-ai-mmm-prod.dhioe.UPDATE_FORECASTING_COST` (
    param_fw_start_date DATE
)

BEGIN 

    SET param_fw_start_date = param_fw_start_date + 7;

    DELETE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FORECAST_COST` 
    WHERE fw_start_date = param_fw_start_date - 7;

    INSERT INTO `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FORECAST_COST` 
    -- CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FORECAST_COST` AS

    WITH PARAM AS (
        SELECT DISTINCT 
            campaign_code,
            campaign_name,
            campaign_type,
            period_description,
            week_nbr,
            campaign_start_date,
            fw_end_date AS campaign_end_date,
            cmpgn_exec_id,
            IFNULL(redeemer_override_flag, False) AS redeemer_override_flag,
            'SUPERS' AS division_name
        FROM
            `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_campaign_view`
        WHERE
            campaign_code IN (SELECT DISTINCT campaign_code FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.stg_smmm_campaignlist_view` WHERE campaign_type = 'CAT')
            AND week_promo_end BETWEEN (param_fw_start_date - 4) AND (param_fw_start_date + 2)
    ),

    offers AS (
        SELECT DISTINCT 
            tco.campaign_code,
            tco.campaign_start_date,
            tco.offer_id,
            tco.division_name,
            tco.campaign_week_nbr AS week_nbr,
            CASE WHEN LEFT(lms_offer_id,1) = '-' THEN 'Virtual Offer' 
                WHEN LEFT(lms_offer_id,1) = '9' AND length(lms_offer_id)=8 THEN 'EE Offer'
                WHEN (lms_offer_id = ' ' OR lms_offer_id IS NULL) THEN 'No Offer'                          
                ELSE 'LMS Offer' 
            END AS offer_type,
        FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_campaign_offer_view` tco
            INNER JOIN param 
                ON tco.campaign_code = param.campaign_code
                AND DATE(tco.campaign_start_date) = param.campaign_start_date
                AND tco.campaign_week_nbr =  param.week_nbr
                AND tco.division_name = param.division_name
            LEFT JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.campaign_exec_v` ce ON split(param.campaign_code,'_')[OFFSET(0)] = ce.CampaignCode AND param.campaign_start_date = ce.LifecycleStartDate AND IFNULL(ce.TestExecIndicator, 'N') = 'N'
        WHERE LOWER(tco.offer_desc) NOT LIKE '%cvm-0007 sow%'
            AND LOWER(tco.offer_desc) NOT LIKE '%cvm-0007 message%'
            AND LOWER(tco.offer_desc) NOT LIKE '%docket%'
    ), 

    target AS (
        SELECT DISTINCT
            crn,
            campaign_code || '-' || campaign_type AS campaign_code,
            DATE(campaign_start_date) AS campaign_start_date,
            Campaign_Seg_Grp_Id AS campaign_segment
        FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER` 
        WHERE Campaign_Seg_Grp_Id != 'None'
    ),

    fw_cost AS (
        /*cost per crn if redeemed*/
        SELECT  
            week_nbr,
            campaign_code,
            campaign_start_date,
            crn,
            campaign_segment,
            Sum(cost_excld_gst) AS cost_excld_gst,
            Sum(cost_incld_gst) AS cost_incld_gst,
        FROM    (
            --LMS Offers
            SELECT     
                param.week_nbr,
                param.campaign_code,
                param.campaign_start_date,
                lcd.CustomerRegistrationNumber AS crn,
                t.campaign_segment,
                -Sum(CASE WHEN ass.TaxPercent = 0 THEN RewardValue ELSE RewardValue/(1+(ass.TaxPercent/100)) END) AS cost_excld_gst, 
                -Sum(srd.RewardValue) AS cost_incld_gst,
            FROM target t
            INNER JOIN param 
                ON t.campaign_code = param.campaign_code AND t.campaign_start_date = param.campaign_start_date
            INNER JOIN offers 
                ON param.campaign_code = offers.campaign_code 
                AND param.campaign_start_date = offers.campaign_start_date
                AND param.division_name = offers.division_name
                AND param.week_nbr = offers.week_nbr
                AND offers.offer_type =  'LMS Offer' 
            INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v` lcd 
                ON lcd.CustomerRegistrationNumber = t.crn
            INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.sales_reward_detail_v` srd 
                ON srd.OfferID = offers.offer_id 
                AND srd.LoyaltyCardNumber = lcd.LoyaltyCardNumber 
                AND srd.TXNStartDate BETWEEN param_fw_start_date - 7 AND param_fw_start_date - 1 --FW Start and End date to Align with PnL data
                AND srd.TXNStartDate BETWEEN param.campaign_start_date AND param.campaign_end_date
            INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v` ass 
                ON srd.TXNStartDate = ass.TXNStartDate
                AND srd.TXNStartTime = ass.TXNStartTime
                AND srd.Site = ass.Site
                AND srd.POSNumber = ass.POSNumber
                AND srd.POSTXNNumber = ass.POSTXNNumber
                AND srd.ArticleWithUOM = ass.ArticleWithUOM
                AND CAST(srd.SalesOrg AS STRING) = CAST(ass.SalesOrg AS STRING)
                AND ass.VoidFlag <> 'Y' 
            GROUP BY 1,2,3,4,5

            UNION ALL
            
            --Virtual Offers
            SELECT     
                param.week_nbr,
                param.campaign_code,
                param.campaign_start_date,
                cer.CustomerRegistrationNumber AS crn,
                t.campaign_segment,
                Sum(CASE WHEN cer.TaxPercent = 0 THEN cer.RewardAmount ELSE cer.RewardAmount/(1+(cer.TaxPercent/100)) END) AS cost_excld_gst,
                Sum(cer.RewardAmount) AS cost_incld_gst,
            FROM param
            INNER JOIN offers 
                ON param.campaign_code = offers.campaign_code 
                AND param.campaign_start_date = offers.campaign_start_date 
                AND param.division_name = offers.division_name 
                AND param.week_nbr = offers.week_nbr
                AND offers.offer_type =  'Virtual Offer'
            INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.campaign_exec_reward_v` cer 
                ON cer.CampaignExecutionID = param.cmpgn_exec_id  
                AND CAST(cer.OfferID AS STRING) = offers.offer_id 
                AND cer.TXNStartDate BETWEEN param_fw_start_date - 7 AND param_fw_start_date - 1
                AND cer.TXNStartDate BETWEEN param.campaign_start_date AND param.campaign_end_date
            INNER JOIN target t 
                ON t.crn = cer.CustomerRegistrationNumber 
                AND param.campaign_code = t.campaign_code 
                AND param.campaign_start_date = t.campaign_start_date 
            WHERE cer.RewardCreditStatus <> 'Credit Failed'
            GROUP BY 1,2,3,4,5

            UNION ALL
            
            --Cost override(When redeemer_override_flag = 'Y')
            SELECT       
                p.week_nbr,
                p.campaign_code,
                p.campaign_start_date,
                r.crn,
                t.campaign_segment,
                SUM(reward_val) AS cost_excld_gst,
                SUM(IFNULL(reward_val_incld_gst, reward_val)) AS cost_incld_gst,
            FROM         
                param p
                INNER JOIN `gcp-wow-rwds-ai-data-prod.loyalty_bi_analytics.fact_campaign_manual_redeem` r 
                    ON p.campaign_code = r.campaign_code  
                    AND p.campaign_start_date = DATE(r.campaign_start_date)
                    AND p.campaign_name|| ' ' || p.period_description = r.campaign_name
                    AND p.week_nbr = r.campaign_week_nbr
                    AND p.division_name = r.division_name
                    AND p.redeemer_override_flag = True
                INNER JOIN target t 
                    ON r.crn = t.crn 
                    AND p.campaign_code = t.campaign_code 
                    AND p.campaign_start_date = t.campaign_start_date 
            GROUP BY 1,2,3,4,5
            )
        GROUP BY 1,2,3,4,5   
    )

    SELECT 
        param_fw_start_date - 7 AS fw_start_date, 
        campaign_start_date,
        campaign_segment,
        week_nbr,
        COUNT(DISTINCT crn) AS redeemers,
        SUM(cost_excld_gst) AS cost
    FROM fw_cost 
    GROUP BY 1, 2, 3, 4
    ORDER BY 1, 2, 4, 4;

END;