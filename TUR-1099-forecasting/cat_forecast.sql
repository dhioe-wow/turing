DECLARE latest_date DATE;
SET latest_date = (
    SELECT MAX(offer_start_date) 
    FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_solus_view` fcs
    WHERE campaign_code IN (
        SELECT DISTINCT campaign_code || '-' || campaign_type
        FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
    )
);

WITH dates_campaign AS (
    SELECT * 
    FROM (
        SELECT DISTINCT fw_start_date
        FROM `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.dim_date`
        WHERE fw_start_date >= '2021-04-19'
            AND fw_start_date <= latest_date
    )
        CROSS JOIN (
            SELECT DISTINCT 
                Campaign_Seg_Grp_Id
            FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
            WHERE Campaign_Seg_Grp_Id != 'None'
        )
    GROUP BY 1, 2
),

audience AS (
    SELECT 
        campaign_start_date,
        Campaign_Seg_Grp_Id,
        COUNT(DISTINCT crn) AS audience
    FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
    GROUP BY 1,2
),

promo AS (
    SELECT 
        ma.Campaign_Seg_Grp_Id,
        ca.campaign_start_date,
        ca.fw_start_date,
        DATE_DIFF(ca.fw_start_date + 1, ca.campaign_start_date, DAY) / 7 + 1 AS campaign_week_nbr,
        SUM(ca.attributed_inc_sales) AS inc_sales,
    FROM
        `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.cp_att_crn_all_view` ca
        JOIN `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER` ma
            ON ca.crn = ma.crn
            AND ca.campaign_code = ma.campaign_code || '-' || ma.campaign_type
            AND ca.campaign_start_date = DATE(ma.campaign_start_date)
    WHERE fw_start_date >= '2021-04-19'
        AND fw_start_date <= latest_date
        AND Campaign_Seg_Grp_Id != 'None'
    GROUP BY 1,2,3,4
), 

post AS (
    SELECT  
        ma.Campaign_Seg_Grp_Id,
        ca.campaign_start_date,
        ca.fw_start_date,
        DATE_DIFF(ca.fw_start_date + 1, ca.campaign_start_date, DAY) / 7 + 1 AS campaign_week_nbr,
        SUM(ca.inc_sales) AS inc_sales,
    FROM 
        `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.cp_att_crn_halo_post_all_view` ca
        JOIN `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER` ma
            ON ca.crn = ma.crn
            AND ca.campaign_code = ma.campaign_code || '-' || ma.campaign_type
            AND ca.campaign_start_date = DATE(ma.campaign_start_date)
    WHERE fw_start_date >= '2021-04-19'
        AND fw_start_date <= latest_date
        AND Campaign_Seg_Grp_Id != 'None'
    GROUP BY 1,2,3,4
),

_promo_post AS (
    SELECT * FROM promo 
    UNION ALL
    SELECT * FROM post
),

promo_post AS (
    SELECT
        pp.*, a.audience
    FROM _promo_post pp
        JOIN audience a 
            ON pp.Campaign_Seg_Grp_Id = a.Campaign_Seg_Grp_Id
            AND pp.campaign_start_date = DATE(a.campaign_start_date)
)

SELECT
    dc.fw_start_date AS ds,
    dc.Campaign_Seg_Grp_Id AS offer_type,
    campaign_week_nbr,
    SUM(audience) AS audience,
    SUM(inc_sales) AS inc_sales,
    SUM(cost) AS cost,
    SUM(redeemers) AS redeemers
FROM 
    dates_campaign dc
    LEFT JOIN promo_post pp
        ON dc.fw_start_date = pp.fw_start_date
        AND dc.Campaign_Seg_Grp_Id = pp.Campaign_Seg_Grp_Id
    LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FORECAST_COST` fc 
        ON dc.fw_start_date = fc.fw_start_date
        AND dc.Campaign_Seg_Grp_Id = fc.campaign_segment
GROUP BY 1, 2, 3
ORDER BY 1, 2, 3;