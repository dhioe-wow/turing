import os
import shutil
import subprocess
from datetime import datetime
import time
import json, yaml, tarfile
import logging


from google.cloud import bigquery
from google.cloud import storage
import pandas as pd

gcp_cred = f"/home/jovyan/.config/gcloud/legacy_credentials/"
gcp_cred += f"{os.getenv('JUPYTERHUB_USER')}/adc.json"
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = gcp_cred


class ScoringBase():
    ''' Base class for scoring utility
    '''
    def __init__(self, project, bucket, prefix, model_path, base_sql_command):
        '''Base class '''
        self.project = project
        self.bucket = bucket
        self.prefix = f'{prefix}/scoring'
        self.local_path = f'local/{prefix}/scoring'
        self.feature_path = f'{model_path}/diagnosis'
        self.model_path = f'{model_path}/model_selector/model_object.pickle'
        self.base_sql_command = base_sql_command
        
    
    def get_sql(self):
        self.sql_command = 'with base as (' + self.base_sql_command + ') select base.* from base;'


    def upload2gcs(self, origin, dest):
        '''upload to GCS'''
        client = storage.Client(project=self.project)
        bucket =client.get_bucket(self.bucket)
        blob = bucket.blob(dest)
        blob.upload_from_filename(origin)
        
        
    def preprocess_func(self, df):
        '''Customise your script here. Implemented in child class.'''
        return df
    
    
    def get_source(self):
        logging.info("Preparing source file...")
        
        # get the features used by the model
        logging.info('Getting model features...')
        best_iteration = pd.read_json(self.feature_path + '/FeatureSelectionSummary.json')['Best'][0]
        df_model_feature = pd.read_excel(self.feature_path + '/FeatureSelectionSummary.xlsx', sheet_name="Feature Records")
        df_model_feature.fillna(9999, inplace=True)
        model_feature_list = df_model_feature[df_model_feature['Drop at iteration'] >= best_iteration]['feature_name'].to_list()
        model_feature_list = [f[3:] if f.startswith('f0_') else f for f in model_feature_list] # cleaning up f0

        # get complete customer features
        logging.info('Getting customer features...')
        sql = self.sql_command
        bq = bigquery.Client(project=self.project)
        df_cust_feature = bq.query(sql).result().to_dataframe()
        df_cust_feature = self.preprocess_func(df_cust_feature)
        cust_feature_list = df_cust_feature.columns.to_list()

        model_cust_feature_list = list(set(model_feature_list).intersection(set(cust_feature_list))) # get customers features used in the model
        cmd_feature_list = [i for i in model_feature_list if i not in cust_feature_list] # get features drawn from CMD

        # write to source 
        with open(self.local_path+'/source.txt', 'w') as f:
            for i in cmd_feature_list:
                f.write(i + '\n') 

        # save to GCS
        logging.info('Saving source.txt to GCS...')
        self.upload2gcs(self.local_path+'/source.txt', self.prefix+'/source.txt')
        self.feature_list = model_cust_feature_list
        
        
    def get_cmd_input(self):
         # generate input parquet
        sql_fields = ', ' + ', '.join(self.feature_list)
        sql_select = 'crn, ref_dt{sql_fields}'.format(sql_fields=sql_fields)
  
        sql = self.sql_command.replace('base.*', sql_select)
        logging.debug(f'Resulting SQL query: {sql}')

        bq = bigquery.Client(project=self.project)
        output_df = bq.query(sql).result().to_dataframe()
        logging.debug('Sample data to be written to GCS:')
        logging.debug(output_df.head())

        logging.info('Saving input.parquet to GCS...')
        output_df.to_parquet(f'gs://{self.bucket}/{self.prefix}/' + 'input.parquet', index=False)


    def get_config(self):
        logging.info("Preparing config file...")
        config = {
            "google_project":"wx-bq-poc",
            "google_bucket":"wx-personal",
            "google_directory":f"{self.prefix}/output",
            "run_date":datetime.today().strftime('%Y-%m-%d'),
            "input_location":f"{self.bucket}/{self.prefix}/input",
            "input_type":"path",
            "udf_location":f"{self.bucket}/{self.prefix}/dist_udf.tar.gz",
            "udf_name":"dist_udf",
            "chunks":"40",
            "udf_output_path":"output",
            "combine":"Y"
        }

        with open(self.local_path+'/config.json', 'w') as f:
            json.dump(config, f)
        self.upload2gcs(self.local_path+'/config.json', self.prefix+'/config.json')


    def get_udf_work_flow(self):
        logging.info("Preparing UDF work flow file...")
        with open('src/akl_templates/udf_work_flow.yaml', 'r') as f:
            udf_work_flow = yaml.load(f, Loader=yaml.FullLoader)
        udf_work_flow['spec']['arguments']['parameters'][0]['value'] = f'gs://{self.bucket}/{self.prefix}/config.json'
        with open(self.local_path+'/udf_work_flow.yaml', 'w') as f:
            yaml.dump(udf_work_flow, f)


    def get_tar(self):
        logging.info("Preparing tar file...")
        command_list = [
            'pip install --no-cache-dir -r requirement.txt;'
            'pip3 install --no-cache-dir -r requirement.txt;'
            'gcloud source repos clone bitbucket_wx_5Frds_auto_5Fml auto_ml;'
            'cd auto_ml;'
            'git pull;'
            'git fetch --tags;'
            'git checkout tags/0.4;'
            'cd ..;'
            'mv auto_ml/helper .;'
            'mv auto_ml/lgbmfit .;'
            f'gsutil cp {self.model_path} .;'
        ]

        with open('prerun.sh', 'w') as f:
            for command in command_list:
                f.write(command + '\n')

        shutil.copy('src/akl_templates/dist_udf.py', 'dist_udf.py')
        shutil.copy('src/akl_templates/requirement.txt', 'requirement.txt')

        with tarfile.open(self.local_path+'/dist_udf.tar.gz', "w:gz") as tar:
            for name in ['prerun.sh', 'dist_udf.py', 'requirement.txt']:
                tar.add(name)

        os.remove('prerun.sh')
        os.remove('dist_udf.py')
        os.remove('requirement.txt')

        self.upload2gcs(self.local_path+'/dist_udf.tar.gz', self.prefix+'/dist_udf.tar.gz')


    def get_cmd_command(self):

        bash_string = 'gcloud --project wx-bq-poc dataproc workflow-templates instantiate preprod-spark-cmd2-pivoter-v2 --async --region us-east4 --parameters \\\n'
        bash_string += f'INPUT_PATH=gs://{self.bucket}/{self.prefix}/input.parquet,\\\n'
        bash_string += f'INPUT_TYPE=parquet,\\\n'
        bash_string += f'OUTPUT_PATH=gs://{self.bucket}/{self.prefix}/input/,\\\n'
        bash_string += f'OUTPUT_TYPE=parquet,\\\n'
        bash_string += f'CSV_DELIMITER=\'|\',\\\n'
        bash_string += f'PARTITION_FORMAT=yyyy-MM-dd,\\\n'
        bash_string += f'JOIN_TYPE=left,\\\n'
        bash_string += f'DECIMAL_AS_DOUBLE=true,\\\n'
        bash_string += f'FILE_INCLUDED_SOURCES=na,\\\n'
        bash_string += f'FILE_INCLUDED_FEATURES=gs://{self.bucket}/{self.prefix}/source.txt,\\\n'
        bash_string += f'FILE_FEATURE_FILTER=na,\\\n'
        bash_string += f'INCLUDE_FUEL=false,\\\n'
        bash_string += f'NO_SUBFOLDER=true,\\\n'
        bash_string += f'OUT_FILE_NUM=50,\\\n'
        bash_string += f'SPARSITY_UPLIMIT=1'

        return bash_string


    def get_argo_command(self):

        bash_string = 'gcloud --project wx-bq-poc container clusters get-credentials project-melon --zone=us-west1-a \n'
        bash_string += f'/home/jovyan/.local/bin/argo submit {self.local_path}/udf_work_flow.yaml'

        return bash_string


    def detect_end_of_cmd(self, sleep_interval):
        
        cmd_partition_list = []
        while len(cmd_partition_list) != 50:
            cmd_partition_list = []
            logging.info(f'Checking CMD Pivoter @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
            client = storage.Client(project=self.project)
            for blob in client.list_blobs(self.bucket, prefix=f'{self.prefix}/input/'):
                if '.parquet' in blob.name:
                    cmd_partition_list.append(blob.name)

            if len(cmd_partition_list) == 50:
                print(f'CMD Pivoter Finished @ {datetime.now().strftime("%d/%m/%Y %H:%M:%S")}')
                break
                
            time.sleep(60 * sleep_interval)


    def execute_bash(self, bash_string):
        print('='*50)
        print('bash input command:'.center(50))
        print('-'*50)
        print(bash_string)
        print('='*50)
        shell_output = subprocess.check_output(bash_string, stderr=subprocess.STDOUT, shell=True, executable='/bin/bash')
        print('bash output results:'.center(50))
        print('-'*50)
        print(shell_output.decode("utf-8") )
        print('='*50)
                

    def run(self):
        logging.info(f'Preparing artifacts for scoring pipeline. All artifacts are saved in {self.local_path}')
        os.makedirs(os.path.dirname(self.local_path+'/'), exist_ok=True)
        
        self.get_sql()
        self.get_source()
        self.get_cmd_input()
        self.get_config()
        self.get_tar()
        self.get_udf_work_flow()

        cmd_command = self.get_cmd_command()
        argo_command = self.get_argo_command()

        self.execute_bash(cmd_command)
        self.detect_end_of_cmd(1)
        self.execute_bash(argo_command)