from datetime import datetime
import os, sys

from google.cloud import bigquery
from google.cloud import storage
import numpy as np
import pandas as pd

from auto_cmd_akl_utility import AutoCmdAklWrapper
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json"


class BaseCMDAKLFeature:
    def __init__(
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):
        
        assert prefix is not None

        self.project = project
        self.bucket = bucket
        self.prefix = prefix
        self.storage = storage.Client(project=project)
        self.akl_config = akl_config
        self.cmd_config = cmd_config


    def get_cmd_config(self):
        '''Customise your script here'''
        cmd_config = {
            'project': self.project,
            'bucket': self.bucket,
            'prefix': self.prefix,
            'source_file_content': self.cmd_config['source_file_content'],
            'feature_file_content': self.cmd_config['feature_file_content'],
        }
        return cmd_config


    def get_sql_command(self):
        '''Customise your script here'''
        sql_command = ""
        return sql_command


    def get_preprocess_func(self):
        '''Customise your script here'''
        def preprocess_func(df, **kwargs):
            return df
        return preprocess_func

    
    def customize_argo_yaml(argo_dict, args):
        '''Customise your script here'''
        return argo_dict

    
    def customize_conf_yaml(self, conf_dict):
        '''Customise your script here'''
        return conf_dict


    def customize_excel(self, exce_dict):
        '''Customise your script here'''
        return exce_dict
    

    def run(self):
        
        def _overwrite_permission():
            response = input('[yes/no]: ')
            if response.lower() not in ['yes', 'no']:
                print('Answer only with either yes or no.')
                overwrite_permission()
            else:
                return response
            
        gcs_dir = f'gs://{self.project}/{self.bucket}/{self.prefix}/cmd'
        prompt = f'Data {gcs_dir} already exists. Do you wish to overwrite?'
        
        blobs = self.storage.list_blobs(self.bucket, prefix=f'{self.prefix}')
        blobs_list = [blob for blob in blobs]
        if len(blobs_list) > 0:
            print(prompt)
            response = _overwrite_permission()
            if response == 'no':
                print(f'CMD job has been cancelled.')
                sys.exit(0)
        
        sql_command = self.get_sql_command()
        cmd_config = self.get_cmd_config()
        auto_cmd_akl = AutoCmdAklWrapper(**cmd_config)   
        preprocess_func = self.get_preprocess_func()
        
        auto_cmd_akl.create_cmd_inputs('sql', sql_command, preprocess_func)
        print('='*50)
        auto_cmd_akl.excute_cmd()
        print('='*50)
        
        auto_cmd_akl.detect_end_of_cmd(1)
        print('='*50)

        argo_dict, conf_dict, exce_dict = auto_cmd_akl.generate_akl_config_files()
        conf_dict = self.customize_conf_yaml(conf_dict)
        exce_dict = self.customize_excel(exce_dict)
        auto_cmd_akl.create_akl_inputs(argo_dict, conf_dict, exce_dict)
        auto_cmd_akl.excute_akl()


    def run_akl(self, cmd_path='auto'):
        
        cmd_config  = {
            'project': self.project,
            'bucket': self.bucket,
            'prefix': self.prefix,
            'cmd_output_path': cmd_path,
        }
        auto_cmd_akl = AutoCmdAklWrapper(**cmd_config)  
        auto_cmd_akl.detect_end_of_cmd(1)
        print('='*50)

        argo_dict, conf_dict, exce_dict = auto_cmd_akl.generate_akl_config_files()
        conf_dict = self.customize_conf_yaml(conf_dict)
        exce_dict = self.customize_excel(exce_dict)
        auto_cmd_akl.create_akl_inputs(argo_dict, conf_dict, exce_dict)
        auto_cmd_akl.excute_akl()