import os, sys
from datetime import datetime
import numpy as np
import pandas as pd

from cmd_akl_base import BaseCMDAKLFeature


class SS1_CMDAKLFeature(BaseCMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        assert prefix is not None
        if cmd_config is None:
            cmd_config = {
                'source_file_content': [
                    'cmd_b01_01_membership',
                    'cmd_b02_02_txn_dsct',
                    'cmd_b03_03_txn_ctgry',
                    'cmd_b06_04_txn_bskt_scan',
                    'cmd_b07_06_txn_extra',
                    'cmd_b08_07_et_camp',
                    'cmd_b10_08_extra_mem_obit_scan',
                    'cmd_b11_09_txn_subcat',
                    'cmd_b18_13_txn_sup_ts_scan',
                    'cmd_b20_13_txn_time_series_scan',
                    'cmd_b22_17_health',
                    'cmd_b23_18_mem_pref_store',
                    'cmd_b26_21_cust_dist',
                    'cmd_b28_23_et_camp_type',
                    'cmd_b29_24_houshold',
                    'cmd_b30_25_spend_stretch_camp',
                    'cmd_b32_26_txn_dept_scan',
                    'cmd_b33_27_email_open_device',
                    'cmd_b46_39_drivetime'
                ],
                'feature_file_content': None
            }

        super(SS1_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

        self.sample_weight_col_name = 'sample_w'
        self.target_variable_name = akl_config['global']['target']


    def get_sql_command(self):
        sql_command = """
            SELECT * EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
            ) FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060`
            WHERE offer_type = 'SS1'
                AND Model='R'
                AND ref_dt <= '2021-02-28'
        """
        return sql_command


    def get_preprocess_func(self):    

        def preprocess_func(df, **kwargs):            
            df.ref_dt = pd.to_datetime(df.ref_dt)

            # redeem vs. non-redeem imbalance handling
            holdout_df = pd.DataFrame(df.loc[df.ref_dt >= pd.to_datetime('2021-01-01')])
            trainin_df = df.loc[df.ref_dt < pd.to_datetime('2021-01-01')]
            print('redeem_flag counts before downsampling')
            print(trainin_df.redeem_flag.value_counts() / trainin_df.redeem_flag.value_counts().min())
            df_rdm_0 = pd.DataFrame(trainin_df.loc[trainin_df.redeem_flag==0, :])
            df_rdm_1 = pd.DataFrame(trainin_df.loc[trainin_df.redeem_flag==1, :])
            sample_size_orig = df_rdm_0.shape[0]
            sample_size = int(df_rdm_1.shape[0]*10)
            df_rdm_0 = df_rdm_0.sample(n=sample_size, replace=False, random_state=123)
            df_rdm_0[self.sample_weight_col_name] = 1.0 / (sample_size/sample_size_orig)
            df_rdm_1[self.sample_weight_col_name] = 1.0
            holdout_df[self.sample_weight_col_name] = 1.0
            df = pd.concat([df_rdm_0, df_rdm_1, holdout_df], axis=0)
  
            # SQL raw data type conversion #
            df.spend_hurdle = pd.to_numeric(df.spend_hurdle, errors='coerce')
            df.txn_tot_amt_4w = pd.to_numeric(df.txn_tot_amt_4w, errors='coerce')
            df.txn_tot_dscnt_4w = pd.to_numeric(df.txn_tot_dscnt_4w, errors='coerce')
            df.txn_tot_dscnt_perc_4w = pd.to_numeric(df.txn_tot_dscnt_perc_4w, errors='coerce')
            df.txn_tot_amt_2w = pd.to_numeric(df.txn_tot_amt_2w, errors='coerce')
            df.txn_tot_dscnt_2w = pd.to_numeric(df.txn_tot_dscnt_2w, errors='coerce')
            df.txn_tot_dscnt_perc_2w = pd.to_numeric(df.txn_tot_dscnt_perc_2w, errors='coerce')
            df.avg_wkly_amt_4w = pd.to_numeric(df.avg_wkly_amt_4w, errors='coerce')
            df.min_wkly_amt_4w = pd.to_numeric(df.min_wkly_amt_4w, errors='coerce')
            df.max_wkly_amt_4w = pd.to_numeric(df.max_wkly_amt_4w, errors='coerce')
            df.avg_wkly_amt_2w = pd.to_numeric(df.avg_wkly_amt_2w, errors='coerce')
            df.min_wkly_amt_2w = pd.to_numeric(df.min_wkly_amt_2w, errors='coerce')
            df.max_wkly_amt_2w = pd.to_numeric(df.max_wkly_amt_2w, errors='coerce')
            df.l1w_spend_hurdle = pd.to_numeric(df.l1w_spend_hurdle, errors='coerce')
            df.l1w_total_rewards = pd.to_numeric(df.l1w_total_rewards, errors='coerce')
            df.l2w_spend_hurdle = pd.to_numeric(df.l2w_spend_hurdle, errors='coerce')
            df.l2w_total_rewards = pd.to_numeric(df.l2w_total_rewards, errors='coerce')
            df.l3w_spend_hurdle = pd.to_numeric(df.l3w_spend_hurdle, errors='coerce')
            df.l3w_total_rewards = pd.to_numeric(df.l3w_total_rewards, errors='coerce')
            df[self.target_variable_name] = pd.to_numeric(df[self.target_variable_name], errors='coerce')
            
            df.woy = df.woy.astype(str)
            df.doy = df.doy.astype(str)
            df.doy_nnh = df.doy_nnh.astype(str)
            df.woy_nnh = df.woy_nnh.astype(str)
            df.doy_nod = df.doy_nod.astype(str)
            df.woy_nod = df.woy_nod.astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']

        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80
        conf_dict['global']['sample_weight'] = self.sample_weight_col_name

        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.sample_weight_col_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.sample_weight_col_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        constrain_df.loc[0] = [self.target_variable_name, f'f0_{self.target_variable_name}', 'num', '-1000', '1000', '']
                    
        return exce_dict




class SS1_ZIR_CMDAKLFeature(SS1_CMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        super(SS1_ZIR_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

        self.target_variable_name = akl_config['global']['target']


    def get_sql_command(self):
        sql_command = """
            SELECT * EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
            ) FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060`
            WHERE offer_type = 'SS1'
                AND ref_dt <= '2021-02-28'
                AND weekly_inc_sales != 0
        """
        return sql_command


    def get_preprocess_func(self):    

        def preprocess_func(df, **kwargs):            
            df.ref_dt = pd.to_datetime(df.ref_dt)

            # SQL raw data type conversion #
            df.spend_hurdle = pd.to_numeric(df.spend_hurdle, errors='coerce')
            df.txn_tot_amt_4w = pd.to_numeric(df.txn_tot_amt_4w, errors='coerce')
            df.txn_tot_dscnt_4w = pd.to_numeric(df.txn_tot_dscnt_4w, errors='coerce')
            df.txn_tot_dscnt_perc_4w = pd.to_numeric(df.txn_tot_dscnt_perc_4w, errors='coerce')
            df.txn_tot_amt_2w = pd.to_numeric(df.txn_tot_amt_2w, errors='coerce')
            df.txn_tot_dscnt_2w = pd.to_numeric(df.txn_tot_dscnt_2w, errors='coerce')
            df.txn_tot_dscnt_perc_2w = pd.to_numeric(df.txn_tot_dscnt_perc_2w, errors='coerce')
            df.avg_wkly_amt_4w = pd.to_numeric(df.avg_wkly_amt_4w, errors='coerce')
            df.min_wkly_amt_4w = pd.to_numeric(df.min_wkly_amt_4w, errors='coerce')
            df.max_wkly_amt_4w = pd.to_numeric(df.max_wkly_amt_4w, errors='coerce')
            df.avg_wkly_amt_2w = pd.to_numeric(df.avg_wkly_amt_2w, errors='coerce')
            df.min_wkly_amt_2w = pd.to_numeric(df.min_wkly_amt_2w, errors='coerce')
            df.max_wkly_amt_2w = pd.to_numeric(df.max_wkly_amt_2w, errors='coerce')
            df.l1w_spend_hurdle = pd.to_numeric(df.l1w_spend_hurdle, errors='coerce')
            df.l1w_total_rewards = pd.to_numeric(df.l1w_total_rewards, errors='coerce')
            df.l2w_spend_hurdle = pd.to_numeric(df.l2w_spend_hurdle, errors='coerce')
            df.l2w_total_rewards = pd.to_numeric(df.l2w_total_rewards, errors='coerce')
            df.l3w_spend_hurdle = pd.to_numeric(df.l3w_spend_hurdle, errors='coerce')
            df.l3w_total_rewards = pd.to_numeric(df.l3w_total_rewards, errors='coerce')
            df[self.target_variable_name] = pd.to_numeric(df[self.target_variable_name], errors='coerce')
            
            df.woy = df.woy.astype(str)
            df.doy = df.doy.astype(str)
            df.doy_nnh = df.doy_nnh.astype(str)
            df.woy_nnh = df.woy_nnh.astype(str)
            df.doy_nod = df.doy_nod.astype(str)
            df.woy_nod = df.woy_nod.astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']

        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80
        conf_dict['feature_selection']['output_iteration'] = -1
        
        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        constrain_df.loc[0] = [self.target_variable_name, f'f0_{self.target_variable_name}', 'num', '-1000', '1000', '']
                    
        return exce_dict




class SS1_ZIC_CMDAKLFeature(SS1_CMDAKLFeature):

    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        super(SS1_ZIC_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

    def get_sql_command(self):
        sql_command = """
            SELECT * EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
                ,weekly_inc_sales
            ), 
                CASE WHEN weekly_inc_sales = 0 THEN 0 ELSE 1 END AS weekly_inc_sales_flag
            FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060`
            WHERE offer_type = 'SS1'
                AND Model='R'
                AND ref_dt <= '2021-02-28'
        """
        return sql_command
    
    
    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']
        conf_dict['global']['sample_weight'] = self.sample_weight_col_name

        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80
        conf_dict['diagnosis']['metrics'] = ['f1_score','recall_score','binary_error','auc','accuracy_score']
        conf_dict['feature_selection']['output_iteration'] = -1

        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.sample_weight_col_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.sample_weight_col_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        return exce_dict




class SS1_Single_CMDAKLFeature(SS1_CMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        super(SS1_Single_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

        self.target_variable_name = akl_config['global']['target']


    def get_sql_command(self):
        sql_command = """
            WITH campaigns AS (
                SELECT 
                    fw_start_date, 
                    crn, 
                    CONCAT(campaign_code, '-', CAST(campaign_start_date AS STRING)) AS campaign_id
                FROM `wx-bq-poc.loyalty_bi_analytics.cp_att_crn_all`
                WHERE campaign_start_date <= '2021-03-15'
            ), 
            audience AS (
                SELECT 
                    crn, 
                    ref_dt, 
                    campaign_start_date
                FROM  `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` 
                WHERE offer_type = 'SS1' 
                    AND ref_dt <= '2021-02-28'
                    AND Model = 'R'
            ),
            unique_audience AS (
                SELECT 
                    a.crn, 
                    a.ref_dt,
                    COUNT(DISTINCT c.campaign_id) AS count_campaign
                FROM audience a JOIN campaigns c 
                    ON a.crn = c.crn
                    AND c.fw_start_date BETWEEN a.campaign_start_date AND a.campaign_start_date + 7
                GROUP BY 1,2
                HAVING COUNT(DISTINCT c.campaign_id) = 1
            )
            SELECT feat.* EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
            )
            FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` feat
                JOIN unique_audience ua 
                    ON feat.crn = ua.crn AND feat.ref_dt = ua.ref_dt
            WHERE offer_type = 'SS1' 
                AND feat.ref_dt <= '2021-02-28'
        """
        return sql_command


    def get_preprocess_func(self):    

        def preprocess_func(df, **kwargs):            
            # SQL raw data type conversion #
            df.spend_hurdle = pd.to_numeric(df.spend_hurdle, errors='coerce')
            df.txn_tot_amt_4w = pd.to_numeric(df.txn_tot_amt_4w, errors='coerce')
            df.txn_tot_dscnt_4w = pd.to_numeric(df.txn_tot_dscnt_4w, errors='coerce')
            df.txn_tot_dscnt_perc_4w = pd.to_numeric(df.txn_tot_dscnt_perc_4w, errors='coerce')
            df.txn_tot_amt_2w = pd.to_numeric(df.txn_tot_amt_2w, errors='coerce')
            df.txn_tot_dscnt_2w = pd.to_numeric(df.txn_tot_dscnt_2w, errors='coerce')
            df.txn_tot_dscnt_perc_2w = pd.to_numeric(df.txn_tot_dscnt_perc_2w, errors='coerce')
            df.avg_wkly_amt_4w = pd.to_numeric(df.avg_wkly_amt_4w, errors='coerce')
            df.min_wkly_amt_4w = pd.to_numeric(df.min_wkly_amt_4w, errors='coerce')
            df.max_wkly_amt_4w = pd.to_numeric(df.max_wkly_amt_4w, errors='coerce')
            df.avg_wkly_amt_2w = pd.to_numeric(df.avg_wkly_amt_2w, errors='coerce')
            df.min_wkly_amt_2w = pd.to_numeric(df.min_wkly_amt_2w, errors='coerce')
            df.max_wkly_amt_2w = pd.to_numeric(df.max_wkly_amt_2w, errors='coerce')
            df.l1w_spend_hurdle = pd.to_numeric(df.l1w_spend_hurdle, errors='coerce')
            df.l1w_total_rewards = pd.to_numeric(df.l1w_total_rewards, errors='coerce')
            df.l2w_spend_hurdle = pd.to_numeric(df.l2w_spend_hurdle, errors='coerce')
            df.l2w_total_rewards = pd.to_numeric(df.l2w_total_rewards, errors='coerce')
            df.l3w_spend_hurdle = pd.to_numeric(df.l3w_spend_hurdle, errors='coerce')
            df.l3w_total_rewards = pd.to_numeric(df.l3w_total_rewards, errors='coerce')
            df[self.target_variable_name] = pd.to_numeric(df[self.target_variable_name], errors='coerce')
            
            df.woy = df.woy.astype(str)
            df.doy = df.doy.astype(str)
            df.doy_nnh = df.doy_nnh.astype(str)
            df.woy_nnh = df.woy_nnh.astype(str)
            df.doy_nod = df.doy_nod.astype(str)
            df.woy_nod = df.woy_nod.astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']
        conf_dict['feature_selection']['output_iteration'] = -1
        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80

        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        constrain_df.loc[0] = [self.target_variable_name, f'f0_{self.target_variable_name}', 'num', '-1000', '1000', '']
                    
        return exce_dict




class SS1_Single_ZIR_CMDAKLFeature(SS1_CMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        super(SS1_Single_ZIR_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

        self.target_variable_name = akl_config['global']['target']


    def get_sql_command(self):
        sql_command = """
            WITH campaigns AS (
                SELECT 
                    fw_start_date, 
                    crn, 
                    CONCAT(campaign_code, '-', CAST(campaign_start_date AS STRING)) AS campaign_id
                FROM `wx-bq-poc.loyalty_bi_analytics.cp_att_crn_all`
                WHERE campaign_start_date <= '2021-03-15'
            ), 
            audience AS (
                SELECT 
                    crn, 
                    ref_dt, 
                    campaign_start_date
                FROM  `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` 
                WHERE offer_type = 'SS1' 
                    AND ref_dt <= '2021-02-28'
            ),
            unique_audience AS (
                SELECT 
                    a.crn, 
                    a.ref_dt,
                    COUNT(DISTINCT c.campaign_id) AS count_campaign
                FROM audience a JOIN campaigns c 
                    ON a.crn = c.crn
                    AND c.fw_start_date BETWEEN a.campaign_start_date AND a.campaign_start_date + 7
                GROUP BY 1,2
                HAVING COUNT(DISTINCT c.campaign_id) = 1
            )
            SELECT feat.* EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
            )
            FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` feat
                JOIN unique_audience ua 
                    ON feat.crn = ua.crn AND feat.ref_dt = ua.ref_dt
            WHERE offer_type = 'SS1' 
                AND feat.ref_dt <= '2021-02-28'
        """
        return sql_command


    def get_preprocess_func(self):    

        def preprocess_func(df, **kwargs):            
            # SQL raw data type conversion #
            df.spend_hurdle = pd.to_numeric(df.spend_hurdle, errors='coerce')
            df.txn_tot_amt_4w = pd.to_numeric(df.txn_tot_amt_4w, errors='coerce')
            df.txn_tot_dscnt_4w = pd.to_numeric(df.txn_tot_dscnt_4w, errors='coerce')
            df.txn_tot_dscnt_perc_4w = pd.to_numeric(df.txn_tot_dscnt_perc_4w, errors='coerce')
            df.txn_tot_amt_2w = pd.to_numeric(df.txn_tot_amt_2w, errors='coerce')
            df.txn_tot_dscnt_2w = pd.to_numeric(df.txn_tot_dscnt_2w, errors='coerce')
            df.txn_tot_dscnt_perc_2w = pd.to_numeric(df.txn_tot_dscnt_perc_2w, errors='coerce')
            df.avg_wkly_amt_4w = pd.to_numeric(df.avg_wkly_amt_4w, errors='coerce')
            df.min_wkly_amt_4w = pd.to_numeric(df.min_wkly_amt_4w, errors='coerce')
            df.max_wkly_amt_4w = pd.to_numeric(df.max_wkly_amt_4w, errors='coerce')
            df.avg_wkly_amt_2w = pd.to_numeric(df.avg_wkly_amt_2w, errors='coerce')
            df.min_wkly_amt_2w = pd.to_numeric(df.min_wkly_amt_2w, errors='coerce')
            df.max_wkly_amt_2w = pd.to_numeric(df.max_wkly_amt_2w, errors='coerce')
            df.l1w_spend_hurdle = pd.to_numeric(df.l1w_spend_hurdle, errors='coerce')
            df.l1w_total_rewards = pd.to_numeric(df.l1w_total_rewards, errors='coerce')
            df.l2w_spend_hurdle = pd.to_numeric(df.l2w_spend_hurdle, errors='coerce')
            df.l2w_total_rewards = pd.to_numeric(df.l2w_total_rewards, errors='coerce')
            df.l3w_spend_hurdle = pd.to_numeric(df.l3w_spend_hurdle, errors='coerce')
            df.l3w_total_rewards = pd.to_numeric(df.l3w_total_rewards, errors='coerce')
            df[self.target_variable_name] = pd.to_numeric(df[self.target_variable_name], errors='coerce')
            
            df.woy = df.woy.astype(str)
            df.doy = df.doy.astype(str)
            df.doy_nnh = df.doy_nnh.astype(str)
            df.woy_nnh = df.woy_nnh.astype(str)
            df.doy_nod = df.doy_nod.astype(str)
            df.woy_nod = df.woy_nod.astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']

        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80
        conf_dict['diagnosis']['metrics'] = ['rmse', 'mse']
        conf_dict['feature_selection']['output_iteration'] = -1

        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        return exce_dict




class SS1_Single_ZIC_CMDAKLFeature(SS1_CMDAKLFeature):
    def __init__( 
        self, 
        project='wx-bq-poc', 
        bucket='wx-personal', 
        prefix=None,
        akl_config=None,
        cmd_config=None
    ):

        super(SS1_Single_ZIC_CMDAKLFeature, self).__init__(
            project=project, 
            bucket=bucket, 
            prefix=prefix,
            akl_config=akl_config,
            cmd_config=cmd_config
        )

        self.target_variable_name = akl_config['global']['target']


    def get_sql_command(self):
        sql_command = """
            WITH campaigns AS (
                SELECT 
                    fw_start_date, 
                    crn, 
                    CONCAT(campaign_code, '-', CAST(campaign_start_date AS STRING)) AS campaign_id
                FROM `wx-bq-poc.loyalty_bi_analytics.cp_att_crn_all`
                WHERE campaign_start_date <= '2021-03-15'
            ), 
            audience AS (
                SELECT 
                    crn, 
                    ref_dt, 
                    campaign_start_date
                FROM  `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` 
                WHERE offer_type = 'SS1' 
                    AND ref_dt <= '2021-02-28'
                    AND Model = 'R'
            ),
            unique_audience AS (
                SELECT 
                    a.crn, 
                    a.ref_dt,
                    COUNT(DISTINCT c.campaign_id) AS count_campaign
                FROM audience a JOIN campaigns c 
                    ON a.crn = c.crn
                    AND c.fw_start_date BETWEEN a.campaign_start_date AND a.campaign_start_date + 7
                GROUP BY 1,2
                HAVING COUNT(DISTINCT c.campaign_id) = 1
            )
            SELECT feat.* EXCEPT(
                offer_type
                ,Model
                ,campaign_code
                ,campaign_length
                ,campaign_start_date
                ,Template_id
                ,partition_date
                ,weekly_spd
                ,as_wkly_spd_sum
                ,as_wkly_spd_avg
                ,weekly_inc_sales
            ), 
                CASE WHEN weekly_inc_sales = 0 THEN 0 ELSE 1 END AS weekly_inc_sales_flag
            FROM `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` feat
                JOIN unique_audience ua 
                    ON feat.crn = ua.crn AND feat.ref_dt = ua.ref_dt
            WHERE offer_type = 'SS1' 
                AND feat.ref_dt <= '2021-02-28'
        """
        return sql_command


    def get_preprocess_func(self):    

        def preprocess_func(df, **kwargs):            
            # SQL raw data type conversion #
            df.spend_hurdle = pd.to_numeric(df.spend_hurdle, errors='coerce')
            df.txn_tot_amt_4w = pd.to_numeric(df.txn_tot_amt_4w, errors='coerce')
            df.txn_tot_dscnt_4w = pd.to_numeric(df.txn_tot_dscnt_4w, errors='coerce')
            df.txn_tot_dscnt_perc_4w = pd.to_numeric(df.txn_tot_dscnt_perc_4w, errors='coerce')
            df.txn_tot_amt_2w = pd.to_numeric(df.txn_tot_amt_2w, errors='coerce')
            df.txn_tot_dscnt_2w = pd.to_numeric(df.txn_tot_dscnt_2w, errors='coerce')
            df.txn_tot_dscnt_perc_2w = pd.to_numeric(df.txn_tot_dscnt_perc_2w, errors='coerce')
            df.avg_wkly_amt_4w = pd.to_numeric(df.avg_wkly_amt_4w, errors='coerce')
            df.min_wkly_amt_4w = pd.to_numeric(df.min_wkly_amt_4w, errors='coerce')
            df.max_wkly_amt_4w = pd.to_numeric(df.max_wkly_amt_4w, errors='coerce')
            df.avg_wkly_amt_2w = pd.to_numeric(df.avg_wkly_amt_2w, errors='coerce')
            df.min_wkly_amt_2w = pd.to_numeric(df.min_wkly_amt_2w, errors='coerce')
            df.max_wkly_amt_2w = pd.to_numeric(df.max_wkly_amt_2w, errors='coerce')
            df.l1w_spend_hurdle = pd.to_numeric(df.l1w_spend_hurdle, errors='coerce')
            df.l1w_total_rewards = pd.to_numeric(df.l1w_total_rewards, errors='coerce')
            df.l2w_spend_hurdle = pd.to_numeric(df.l2w_spend_hurdle, errors='coerce')
            df.l2w_total_rewards = pd.to_numeric(df.l2w_total_rewards, errors='coerce')
            df.l3w_spend_hurdle = pd.to_numeric(df.l3w_spend_hurdle, errors='coerce')
            df.l3w_total_rewards = pd.to_numeric(df.l3w_total_rewards, errors='coerce')
            df[self.target_variable_name] = pd.to_numeric(df[self.target_variable_name], errors='coerce')
            
            df.woy = df.woy.astype(str)
            df.doy = df.doy.astype(str)
            df.doy_nnh = df.doy_nnh.astype(str)
            df.woy_nnh = df.woy_nnh.astype(str)
            df.doy_nod = df.doy_nod.astype(str)
            df.woy_nod = df.woy_nod.astype(str)
            
            return df

        return preprocess_func


    def customize_conf_yaml(self, conf_dict):
        conf_dict['global']['run_date'] = datetime.today().strftime('%Y-%m-%d')
        conf_dict['global']['modeller'] = self.akl_config['global']['modeller']
        conf_dict['global']['objective'] = self.akl_config['global']['objective']
        conf_dict['global']['target'] = self.akl_config['global']['target']
        conf_dict['global']['metric'] = self.akl_config['global']['metric']

        conf_dict['preprocessor']['params']['train_test_holdout']['split_type'] = 'by_time'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeEndDate'] = '2021-01-01'
        conf_dict['preprocessor']['params']['train_test_holdout']['by_time']['inTimeTrainTestSplit'] = 80
        conf_dict['diagnosis']['metrics'] = ['f1_score','recall_score','binary_error','auc','accuracy_score']
        conf_dict['feature_selection']['output_iteration'] = -1

        return conf_dict


    def customize_excel(self, exce_dict):
        mars_data_dictionary_df = exce_dict['mars_data_dictionary']
        feature_spec_num_df = exce_dict['feature_spec_num']
        feature_spec_cat_df = exce_dict['feature_spec_cat']
        constrain_df = exce_dict['Constrain']
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_num_df.loc[feature_spec_num_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == self.target_variable_name, 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature.str.contains('_flx_'), 'input'] = 'ignore'
        
        feature_spec_num_df.loc[feature_spec_num_df.feature == 'redeem_flag', 'input'] = 'ignore'
        feature_spec_cat_df.loc[feature_spec_cat_df.feature == 'redeem_flag', 'input'] = 'ignore'
        return exce_dict