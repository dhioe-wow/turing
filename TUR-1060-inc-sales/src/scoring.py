import argparse
from scoring_base import ScoringBase
import shutil
import pandas as pd


class Scoring(ScoringBase):
    
    def __init__(self, project, bucket, prefix, model_path, base_sql_command):

        super(Scoring, self).__init__(
            project = project,
            bucket = bucket,
            prefix = prefix,
            model_path = model_path,
            base_sql_command = base_sql_command
        )
        
    
    def preprocess_func(self, df):
        '''Customise if needed'''
        return df

    
if __name__ == '__main__':
    # Specify all inputs here
    PROJECT = 'wx-bq-poc'
    BUCKET = 'wx-personal'
    PREFIX = 'denny/direct_inc_sales_model/2C_old'

    MODEL_PATH = 'gs://wx-personal/Denny/2C_spd_direct_202105/4_akl_outputs/v04_v02/2C_spd_direct_202105/2021-05-17'

    SQL_COMMAND = '''
        select 
            * 
        from  `gcp-wow-rwds-ai-mmm-super-dev.DEV_MMM.MMM_FINAL_FEATURES_CAMP_TUR1060` 
        where offer_type='2C' 
            and ref_dt >= '2021-06-01'
        limit 1000
    '''
    
    scoring = Scoring(
        project=PROJECT, 
        bucket=BUCKET, 
        prefix=PREFIX,
        model_path=MODEL_PATH, 
        base_sql_command=SQL_COMMAND, 
    )
    
    scoring.run()