-- This script should be customisable

SELECT DISTINCT 
    base_audience.crn, 
    MOD(CAST(base_audience.crn AS INT64), 3) AS ranking_segment
FROM `gcp-wow-rwds-ai-mmm-prod.dhioe._TEMP_BASE_AUDIENCE_<HASH>` AS base_audience
    JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe._TEMP_BASE_SEGMENT_<HASH>` AS base_segment
    ON base_audience.crn = base_segment.crn;
    
