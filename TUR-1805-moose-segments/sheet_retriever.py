import gspread
import pandas as pd

class SheetRetriever:

    def __init__(self):
        self._gspread = gspread.oauth(flow=gspread.auth.console_flow)

    def retrieve_sheet(self, file_url, sheet_names, enforce_column_types=None):
        google_sheet = self._gspread.open_by_url(file_url)
        result_dfs = []
        for index, sheet in enumerate(sheet_names):
            data = google_sheet.worksheet(sheet).get_all_values()
            headers = data.pop(0)
            df = pd.DataFrame(data, columns=headers)
            if enforce_column_types:
                types = enforce_column_types[index]
                for column, type_ in zip(df.columns, types):
                    if type_ == "datetime64":
                        if re.match(r"^\d{2}-\d{2}-\d{4}", df[column].iloc[0]):
                            df[column] = pd.to_datetime(
                                df[column], format="%d-%m-%Y"
                            )
                        else:
                            df[column] = pd.to_datetime(df[column])
                    df[column] = df[column].astype(type_, errors="ignore")
            result_dfs.append(df)
        return result_dfs