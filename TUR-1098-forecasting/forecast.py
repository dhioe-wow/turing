import os
import logging

import matplotlib.pyplot as plt
import numpy as np

from statsmodels.tsa.exponential_smoothing.ets import ETSModel
import pmdarima as pm
from prophet import Prophet

logging.getLogger("prophet").setLevel(logging.ERROR)
logging.getLogger("pmdarima").setLevel(logging.ERROR)


class suppress_stdout_stderr(object):
    '''
    A context manager for doing a "deep suppression" of stdout and stderr in
    Python, i.e. will suppress all print, even if the print originates in a
    compiled C/Fortran sub-function.
       This will not suppress raised exceptions, since exceptions are printed
    to stderr just before a script exits, and after the context manager has
    exited (at least, I think that is why it lets exceptions through).

    '''
    def __init__(self):
        # Open a pair of null files
        self.null_fds = [os.open(os.devnull, os.O_RDWR) for x in range(2)]
        # Save the actual stdout (1) and stderr (2) file descriptors.
        self.save_fds = (os.dup(1), os.dup(2))

    def __enter__(self):
        # Assign the null pointers to stdout and stderr.
        os.dup2(self.null_fds[0], 1)
        os.dup2(self.null_fds[1], 2)

    def __exit__(self, *_):
        # Re-assign the real stdout/stderr back to (1) and (2)
        os.dup2(self.save_fds[0], 1)
        os.dup2(self.save_fds[1], 2)
        # Close the null files
        os.close(self.null_fds[0])
        os.close(self.null_fds[1])



class Forecast:
    
    def __init__(self, data, offer_type, metric, window):
        self.tag = 'base'
        self.offer_type = offer_type
        self.metric = metric
        self.window = window
        self.df = self.preprocess(data)
        
        
    def fortnight_impute(self, df):
        
        def _fortnight_impute(row):
            if np.isnan(row['y']) and ~np.isnan(row['z']):
                return 0
            else:
                return row['y']

        if self.offer_type in ['SS1', 'MP1', 'TMP']:
            if self.metric in ['rdm', 'cpc']:
                df['z'] = df['y'].shift(1)
                df['y'] = df.apply(_fortnight_impute, axis=1)

        return df[['ds', 'y']]
    
    
    def prophet_impute(self, df):
        
        def _prophet_impute(row):
            if np.isnan(row['y']):
                return row['yhat']
            else:
                return row['y']

        # use prophet to interpolate
        m = Prophet()
        m.add_seasonality(name='2-week', period=14, fourier_order=2)
        with suppress_stdout_stderr():
            m.fit(df)

        future = m.make_future_dataframe(periods=0, freq='w')
        forecast = m.predict(future)
        
        if self.offer_type == 'TMP':
            forecast['yhat'] = forecast['2-week']
        
        forecast = forecast[['ds', 'yhat']]
        forecast['ds'] = forecast['ds'].apply(lambda x: x.date())

        df = df.merge(forecast, how='inner', left_on='ds', right_on='ds')
        df['y'] = df.apply(_prophet_impute, axis=1)

        return df[['ds', 'y']]


    def negative_impute(self, df):
        # replace negative value with zero for positive metrics
        if self.metric in ['rdm', 'cpc', 'promo_post_audience', 'promo_audience']:
            df['y'] = df['y'].apply(lambda x: 0 if x < 0 else x) 
        return df[['ds', 'y']]
    
    
    def preprocess(self, data):
        df = data.copy()
        df = df[df['offer_type']==self.offer_type]
        df = df[['ds', self.metric]]
        df.rename(
            columns={
                self.metric: 'y',
            }, inplace=True
        )
        df = self.fortnight_impute(df)
        df = self.prophet_impute(df)
        df = self.negative_impute(df)
        
        return df
    
    
    def plot_forecast(self):
        try:
            ds = self.df.ds
            y = self.df.y
            yhat = self.predict()
            
            plt.figure(figsize=(20,5))
            plt.title(f"{self.offer_type} - {self.metric}")
            plt.plot(ds[:-self.window], y[:-self.window], label='data (train)')
            plt.plot(ds[-self.window:], y[-self.window:], label='data (test)')
            plt.plot(ds[-self.window:], yhat, label=f'pred ({self.tag})')
            plt.legend()
            plt.grid()
            plt.show()
        except:
            print("Cannot be predicted...")
            
            
    def evaluate(self):
        try:
            yhat = self.predict()
            y = self.df.y[-self.window:]
            sMAPE = ForecastES.eval_smape(y, yhat)
        except:
            sMAPE = 100
        return sMAPE
    
                
    @staticmethod
    def eval_smape(y, yhat):
        return np.mean(np.abs(yhat - y) / (np.abs(yhat) + np.abs(y))) * 100
    

class ForecastES(Forecast):
    
    def __init__(self, data, offer_type, metric, window):
        super().__init__(
            data=data, 
            offer_type=offer_type,
            metric=metric,
            window=window
        )
        self.tag = 'es-issm'
        
        
    def train(self):
        try:
            model = ETSModel(
                self.df.y[:-self.window],
                error="add",
                trend="add",
                seasonal="add",
                seasonal_periods=2,
                initialization_method="estimated",
            )
            self.model = model.fit(disp=0)
        except:
            self.model = None
            return self.model
        
    def predict(self):
        pred = self.model.get_prediction(start=len(self.df.y)-self.window, end=len(self.df.y)-1)
        yhat = pred.summary_frame(alpha=0.05)['mean']
        if self.metric in ['rdm', 'cpc', 'promo_post_audience', 'promo_audience']:
            yhat = yhat.apply(lambda x: 0 if x < 0 else x)  
        return np.array(yhat)  
        
        
class ForecastNaive(Forecast):
    
    def __init__(self, data, offer_type, metric, window):
        super().__init__(
            data=data, 
            offer_type=offer_type,
            metric=metric,
            window=window
        )
        self.tag = 'naive+'
        
        
    def train(self):
                
        def _forecast_next(u, coeff):
            
            pad = [np.nan]*(2*self.window-len(u))
            u = pad + u
            
            y = [coeff[i]**i*u[-2*(i+1)] for i in range(len(coeff))]
            
            # handling nan
            coeff = coeff*(~np.isnan(y))
            y = [0 if np.isnan(y_i) else y_i for y_i in y]

            return np.sum(y) / np.sum(coeff)
        
        coeff = [ 0.5 ** i for i in range(self.window)]
        self.output = list(self.df.y[:-self.window])
                
        for i in range(self.window):
            next_y = _forecast_next(self.output, coeff)
            
            if (self.metric in ['rdm', 'cpc', 'promo_post_audience', 'promo_audience']) and (next_y < 0):
                self.output.append(0)
            else:
                self.output.append(next_y)
                
        return self.output
    
    
    def predict(self):
        return np.array(self.output[-self.window:])
        
        
class ForecastARIMA(Forecast):
    
    def __init__(self, data, offer_type, metric, window):
        super().__init__(
            data=data, 
            offer_type=offer_type,
            metric=metric,
            window=window
        )
        self.tag = 'auto-arima'
        
        
    def train(self):
        try:
            self.model = pm.auto_arima(
                self.df.y[:-self.window], 
                start_p=1,
                start_q=1,
                start_P=0,
                m=2, 
                seasonal=True,
                error_action="ignore") 
            return self.model
        
        except:
            self.model = None
            return self.model

    
    def predict(self):
        yhat = self.model.predict(self.window)
        if self.metric in ['rdm', 'cpc', 'promo_post_audience', 'promo_audience']:
            for i in range(len(yhat)):
                if yhat[i] < 0:
                    yhat[i] = 0           
        return np.array(yhat)
        
        
class ForecastProphet(Forecast):
    
    def __init__(self, data, offer_type, metric, window):
        super().__init__(
            data=data, 
            offer_type=offer_type,
            metric=metric,
            window=window
        )
        self.tag = 'prophet'
        
    
    def train(self):
        try:
            self.model = Prophet()
            self.model.add_seasonality(name='2-week', period=14, fourier_order=2)
            with suppress_stdout_stderr():
                self.model.fit(self.df[:-self.window])       
            return self.model
        except:
            self.model = None
            return self.model
    
    def predict(self): 
        future = self.model.make_future_dataframe(periods=self.window, freq='w')
        forecast = self.model.predict(future)
        yhat = forecast['yhat'][-self.window:]        
        if self.metric in ['rdm', 'cpc', 'promo_post_audience', 'promo_audience']:
            yhat = yhat.apply(lambda x: 0 if x < 0 else x)  
        return np.array(yhat)