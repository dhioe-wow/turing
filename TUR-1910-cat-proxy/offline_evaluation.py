import pickle
import os
os.environ['GOOGLE_APPLICATION_CREDENTIALS']=f"/home/jovyan/.config/gcloud/legacy_credentials/{os.getenv('JUPYTERHUB_USER')}/adc.json" 

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from google.cloud import bigquery

GCP_PROJECT = 'gcp-wow-rwds-ai-mmm-prod'
GCS_PREFIX = 'gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/mmm_super_cat'
GCS_RANKING_OUTPUT = 'predicted_selected'


class Evaluator:
    
    def __init__(self, gcs_dt, ref_dt, objectives):
        self.bq = bigquery.Client(project=GCP_PROJECT)
        self.gcs_dt = gcs_dt
        self.ref_dt = ref_dt
        self.objectives = objectives
        self.config = self.get_config()
    
    
    def get_config(self):
        config = { i : {'OBJ': i} for i in self.objectives }
        for k in config.keys():
            config[k]['GCS'] = f'{GCS_PREFIX}/{self.gcs_dt}-offline-eval-obj-{config[k]["OBJ"].replace("_","-")}/{GCS_RANKING_OUTPUT}/'
        return config
    
    
    def get_data(self):
        dfs = {}
        for k in self.config.keys():
            print(f'\t Reading GCS for set {k}...')
            dfs[k] = pd.read_parquet(f"{self.config[k]['GCS']}")
        return dfs
    
    
    def get_pbq_data(self, model):
        sql = f'''
            SELECT 
                crn,
                template_id,
                AVG(weekly_inc_sales) AS a_inc_sales
            FROM `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
            WHERE ref_dt = '{self.ref_dt}'
                AND Model = '{model}'
            GROUP BY 1,2
        '''
        print(f'\t Reading MMM_POST_BQ_MASTER_AUDIENCE for set {model}...')
        df = self.bq.query(sql).result().to_dataframe()
        return df
    
    
    def get_intersections(self, dfs, df_random):
        dfs_intersection = {}
        for k in self.config.keys():
            dfs_intersection[k] = df_random.merge(dfs[k], left_on=['crn', 'template_id'], right_on=['crn', 'template_id'])
        return dfs_intersection
    
    @staticmethod
    def apply_percentile_correction(df, by, n=10, plot=False):
        df_sorted = df.sort_values(by=by, ascending=False).reset_index(drop=True)

        df_sorted = df_sorted.reset_index()
        df_sorted['rank'] = (df_sorted['index'] + 1) / len(df_sorted)
        df_sorted['percentile'] = pd.qcut(df_sorted['rank'], n, labels=False) + 1

        df_percentile = df_sorted.groupby('percentile').aggregate({
            'selected': 'sum',
            'selected_inc_sales': 'sum',
        }).reset_index()

        df_percentile['selected_inc_sales'] = df_percentile['selected_inc_sales'] / df_percentile['selected']
        df_percentile['selected'] = df_percentile['selected'] / df_percentile['selected'].sum()

        if plot:
            fig, ax = plt.subplots(2, 1, sharex=True, figsize=(8, 8))
            ax[0].set_title('population distribution')
            ax[0].bar(df_percentile.percentile, df_percentile.selected)
            ax[0].grid()

            ax[1].set_title('safari inc sales distribution')
            ax[1].bar(df_percentile.percentile, df_percentile.selected_inc_sales)
            ax[1].grid()
            plt.show()

        print(f"\t\t Before correction : {(df_percentile['selected_inc_sales'] * df_percentile['selected']).sum():.3f}")
        print(f"\t\t After correction  : {df_percentile['selected_inc_sales'].sum() / n :.3f}")
            
    
    def run(self):
        
        print(f"Running offline evaluation for {self.ref_dt}...")
        print(50*"=")
        print()
        
        print("Reading data...")
        dfs = self.get_data()
        df_random = self.get_pbq_data('R') 
        df_model = self.get_pbq_data('M')
        print()
        
        print("Getting intersections with Random...")
        print(f'- Total population: {df_random.shape[0]}')
        print(f'- Total incremental sales: {df_random.a_inc_sales.sum():.1f}')
        dfs_intersection = self.get_intersections(dfs, df_random)
        for k in self.config.keys():
            print(f"\t Intersection {k}")
            print(f'\t\t Share of population: {dfs_intersection[k].shape[0] / df_random.shape[0]:.3f}')
            print(f'\t\t Share of incremental sales: {dfs_intersection[k].a_inc_sales.sum() / df_random.a_inc_sales.sum():.3f}')
        print()
            
        print("Checking execution results...")
        print(f'\t Model RPC: {df_model.a_inc_sales.sum() / df_model.shape[0]:.3f}')
        print(f'\t Random RPC: {df_random.a_inc_sales.sum() / df_random.shape[0]:.3f}')
        print()
        
        print("Evaluating on intersections...")
        for k in self.config.keys():
            print(f"\t Evaluating set {k}...")
            _df = dfs[k].merge(df_random, on='crn')[['crn', self.config[k]['OBJ'], 'a_inc_sales']]
            _df = _df.merge(dfs_intersection[k][['crn']], how='left', on='crn', indicator=True)
            _df['selected'] = _df['_merge'].apply(lambda x: 1 if x == 'both' else 0)
            _df['selected_inc_sales'] = _df['a_inc_sales'] * _df['selected']
            self.apply_percentile_correction(_df, self.config[k]['OBJ'], 10)
        