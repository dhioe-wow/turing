DECLARE HOLDOUT_START_DATE DATE;
SET HOLDOUT_START_DATE = '2022-02-06';

DROP TABLE IF EXISTS `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_SAMPLED_202204`;
CREATE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_SAMPLED_202204` AS (
    SELECT * EXCEPT (seqnum)
    FROM (
        SELECT *, ROW_NUMBER() OVER (PARTITION BY campaign_seg_grp_id ORDER BY RAND()) as seqnum
        FROM `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_202204`
        WHERE Model ='R'
          AND ref_dt < HOLDOUT_START_DATE
    ) foo
    WHERE seqnum <= 100000
);

DROP TABLE IF EXISTS `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_HOLDOUT_202204`;
CREATE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_HOLDOUT_202204` AS (
    SELECT * EXCEPT (
        avg18wk_per_basket,
        b_0, b_1, b_2, b_3, b_4, b_5, b_6, b_7, b_8, b_9,
        b_10, b_11, b_12, b_13, b_14, b_15, b_16, b_17, b_18, b_19, 
        b_20, b_21, b_22, b_23, b_24, b_25, b_26, b_27, b_28, b_29, 
        b_30, b_31, b_32, b_33, b_34, b_35, b_36, b_37, b_38, b_39,
        b_40, b_41, b_42, b_43, b_44, b_45, b_46, b_47, b_48, b_49, 
        b_50, b_51, b_52, b_53, b_54, b_55, b_56, b_57, b_58, b_59, 
        b_60, b_61, b_62, b_63, b_64, b_65, b_66, b_67, b_68, b_69,
        b_70, b_71, b_72, b_73, b_74, b_75, b_76, b_77, b_78, b_79, 
        b_80, b_81, b_82, b_83, b_84, b_85, b_86, b_87, b_88, b_89, 
        b_90, b_91, b_92, b_93, b_94, b_95, b_96, b_97, b_98, b_99,
        b_100, b_101, b_102, b_103, b_104, b_105, b_106, b_107, b_108, b_109, 
        b_110, b_111, b_112, b_113, b_114, b_115, b_116, b_117, b_118, b_119, 
        b_120, b_121, b_122, b_123, b_124, b_125, b_126, b_127
    )
    FROM  `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP_202204`
    WHERE ref_dt >= HOLDOUT_START_DATE
 );