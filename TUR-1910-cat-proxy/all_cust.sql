--==============================================================
/*
SCRIPT:
    all_cust_features.sql

PURPOSE:
    - The SQL code below rolls up the MMM_POST_BQ_MASTER_AUDIENCE containing the targets and
    - joins to the relevant custom / user defined features for the given ref_dt
    - the final centralised table can be used for model retraining
    - Outputs of the code will be stored in `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP`

STEPS:
    0.0 Load Category Embeddings
    1.0 Build campaign history features
    2.0 Build past transactional history features
    3.0 Evaluate the Scanner / booster segment
    4.0 Build non-email channel features
    5.0 Evaluate seasonality / relative holiday / event features
    6.0 Extract last MMM execution features (what campaign customer received, response)
    7.0 Extract the last 18 weeks average spend for each crn,ref_dt,Campaign_Seg_Grp_Id
    (Note: This query may take longer time)
    8.0 Output table - crn, campaign_start_date level

INPUTS:
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
    `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view`
    `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_solus_view`
    `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_weekly_saver_view`
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_date_v`
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v`
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_article_master_v`
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.et_resp_sendlog_v`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.public_holidays`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.marketing_events`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.public_holidays`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_TEST_CARDS`
    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_CAT_ARTICLE_MASTER`
    "gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/mmm_super_cat/<DATE>/outbound/embeddings_by_campaign_segment_group.parquet"


INTERIM TABLES:
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_campaign_hist_features_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_txn_features_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_booster_status_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_HOLIDAY_FEATURES_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FEATURES_FROM_PAST_EXECUTIONS_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_booster_status_202204`
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_category_embeddings_202204`

OUTPUTS:
    `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP`

RUNTIME:

DEVELOPMENT:
    Version 1.0 - Melanie & Ajnas | Apr 2021
    Version 1.1 - Jacqueline | May 2021
    ------------------------------------------
    Copied the script from MMM_SUPER TO MMM_SUPER_CAT

    Version 2.0 - Satabdi | Aug 2021
    Version 2.1 - S Kumar | Oct 2021
*/
--==============================================================

------------------------------------------------------------------------------------------------
-- 0.0 Load Category Embeddings
------------------------------------------------------------------------------------------------
/*
Category embeddings are not static anymore, we suggest to get the list of GCS directories and ref_dt,
e.g., in Python:

  GCS_DIRS = [
    ("2022-01-24-raji-prod-01", "2022-01-09"),
    ("2021-12-20-raji-prod-1", "2021-12-12"),
    ("2021-12-06-sr-prod-1", "2021-11-28"),
    ("2021-11-22-sr-prod-4", "2021-11-14"),
    ...
  ]

  dfs = []
  for gcs_dir, ref_dt in tqdm(GCS_DIRS):
      df = pd.read_parquet("gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/mmm_super_cat/"+gcs_dir+"/outbound/embeddings_by_campaign_segment_group.parquet")
      df['ref_dt'] = pd.to_datetime(ref_dt).date()
      dfs.append(df)

  embedding_df = pd.concat(dfs, axis=0)

Then export this df to bigquery.
*/
------------------------------------------------------------------------------------------------
DECLARE covid_start_date_1 DATE;
DECLARE covid_end_date_1 DATE;
DECLARE covid_length_1 INT64;
DECLARE covid_start_date_2 DATE;
DECLARE covid_end_date_2 DATE;
DECLARE covid_length_2 INT64;

SET covid_start_date_1 = '2020-03-01';
SET covid_length_1 = 180;
SET covid_end_date_1 = DATE_ADD(covid_start_date_1, INTERVAL covid_length_1 DAY);

SET covid_start_date_2 = '2021-07-01';
SET covid_length_2 = 180;
SET covid_end_date_2 = DATE_ADD(covid_start_date_2, INTERVAL covid_length_2 DAY);


------------------------------------------------------------------------------------------------
-- 1.0 Build campaign history features
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_campaign_hist_features_202204` AS (

  WITH camp_history AS (
    -- get campaign history from FY20 onwards
    SELECT CAST(crn AS STRING) AS crn,
           LEFT(campaign_code,8) AS campaign_code,
           campaign_start_date,
           DATE(insert_datetime) AS insert_date,
           special_sales,
           send_flag,
           click_flag,
           open_flag,
           activate_flag,
           redeem_flag
    FROM   `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view`
    WHERE  campaign_audience_type = 'T'
      AND campaign_code IS NOT NULL
      AND campaign_name IS NOT NULL
      AND campaign_start_date > '2019-07-01'

    UNION ALL

    SELECT CAST(crn AS STRING) AS crn,
           campaign_code,
           campaign_start_date,
           DATE(insert_datetime) AS insert_date,
           division AS special_sales,
           send_flag,
           click_flag,
           open_flag,
           activate_flag,
           redeem_flag
    FROM   `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_solus_view`
    WHERE  campaign_audience_type = 'T'
      AND campaign_code IS NOT NULL
      AND campaign_name IS NOT NULL
      AND campaign_start_date > '2019-07-01'

    UNION ALL

    SELECT CAST(crn AS STRING) AS crn,
           campaign_code,
           campaign_start_date,
           DATE(insert_datetime) AS insert_date,
           special_sales,
           send_flag,
           click_flag,
           open_flag,
           NULL AS activate_flag,
           redeem_flag
    FROM   `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_weekly_saver_view`
    WHERE  campaign_audience_type = 'T'
      AND campaign_code IS NOT NULL
      AND campaign_name IS NOT NULL
      AND campaign_start_date > '2019-07-01'
  ),

  base AS (
    -- base audience (expecting crn, ref_dt)
    -- the durations are considered as follows : for 13 wk, it is 1wk post ref_dt and 12wk pre ref_dt making a total of 13wk
    SELECT crn,
           ref_dt,
           ref_dt + 7 AS lw_ref_dt,
           ref_dt - 7 * 12 + 1 AS l13w_dt,
           ref_dt - 7 * 3 + 1 AS l4w_dt,
           ref_dt - 7 * 1 + 1 AS l2w_dt,
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
  ),

  base_long AS (
    -- prepare boolean logic / flags to simplify calculations
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           CASE
             WHEN ch.insert_date BETWEEN b.l13w_dt AND b.lw_ref_dt
             THEN 1 ELSE 0
             END AS is_13w,
           CASE
             WHEN ch.insert_date BETWEEN b.l4w_dt AND b.lw_ref_dt
             THEN 1 ELSE 0
             END AS is_4w,
           CASE
             WHEN ch.insert_date BETWEEN b.l2w_dt AND b.lw_ref_dt
             THEN 1 ELSE 0
             END AS is_2w,
           ch.campaign_code,
           CASE
             WHEN ch.send_flag IS NOT NULL
             THEN 1 ELSE 0
             END AS send_flag,
           CASE
             WHEN ch.click_flag IS NOT NULL
             THEN 1 ELSE 0
             END AS click_flag,
           CASE
             WHEN ch.open_flag IS NOT NULL
             THEN 1 ELSE 0
             END AS open_flag,
           CASE
             WHEN ch.activate_flag IS NOT NULL
             THEN 1 ELSE 0
             END AS activate_flag,
           CASE
             WHEN ch.redeem_flag IS NOT NULL
             THEN 1 ELSE 0
             END AS redeem_flag
    FROM   base AS b

    LEFT JOIN camp_history AS ch
      ON b.crn = ch.crn
        AND ch.insert_date BETWEEN b.l13w_dt AND b.lw_ref_dt

    -- exclude caltex / fuelco campaigns
    WHERE  COALESCE(ch.special_sales, '') NOT IN ('CALTEX', 'FUELCO')
  ),

  base_calc AS (
    -- calculate / summarise campaign metrics
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           -- 13 week features
           SUM(b.is_13w) AS sum_et_camp_cnt_13w,
           SUM(b.is_13w * b.send_flag) AS sum_et_send_cnt_13w,
           SUM(b.is_13w * b.click_flag) AS sum_et_click_cnt_13w,
           SUM(b.is_13w * b.open_flag) AS sum_et_open_cnt_13w,
           SUM(b.is_13w * b.activate_flag) AS sum_et_activate_cnt_13w,
           SUM(b.is_13w * b.redeem_flag) AS sum_et_redeem_cnt_13w,
           -- 4 week features
           SUM(b.is_4w) AS sum_et_camp_cnt_4w,
           SUM(b.is_4w * b.send_flag) AS sum_et_send_cnt_4w,
           SUM(b.is_4w * b.click_flag) AS sum_et_click_cnt_4w,
           SUM(b.is_4w * b.open_flag) AS sum_et_open_cnt_4w,
           SUM(b.is_4w * b.activate_flag) AS sum_et_activate_cnt_4w,
           SUM(b.is_4w * b.redeem_flag) AS sum_et_redeem_cnt_4w,
           -- 2 week features
           SUM(b.is_2w) AS sum_et_camp_cnt_2w,
           SUM(b.is_2w * b.send_flag) AS sum_et_send_cnt_2w,
           SUM(b.is_2w * b.click_flag) AS sum_et_click_cnt_2w,
           SUM(b.is_2w * b.open_flag) AS sum_et_open_cnt_2w,
           SUM(b.is_2w * b.activate_flag) AS sum_et_activate_cnt_2w,
           SUM(b.is_2w * b.redeem_flag) AS sum_et_redeem_cnt_2w
    FROM   base_long AS b
    GROUP BY
      b.crn,
      b.ref_dt,
      b.lw_ref_dt
  )

  SELECT b.crn,
         b.ref_dt,
         b.lw_ref_dt,
         -- 13 week features
         SAFE_DIVIDE(b.sum_et_activate_cnt_13w, b.sum_et_camp_cnt_13w) AS et_activate_rate_13w,
         SAFE_DIVIDE(b.sum_et_click_cnt_13w, b.sum_et_camp_cnt_13w) AS et_click_rate_13w,
         SAFE_DIVIDE(b.sum_et_open_cnt_13w, b.sum_et_camp_cnt_13w) AS et_open_rate_13w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_13w, b.sum_et_camp_cnt_13w) AS et_redeem_rate_13w,
         SAFE_DIVIDE(b.sum_et_click_cnt_13w, b.sum_et_open_cnt_13w) AS et_click_through_rate_13w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_13w, b.sum_et_activate_cnt_13w) AS et_conversion_rate_13w,
         -- 4 week features
         SAFE_DIVIDE(b.sum_et_activate_cnt_4w, b.sum_et_camp_cnt_4w) AS et_activate_rate_4w,
         SAFE_DIVIDE(b.sum_et_click_cnt_4w, b.sum_et_camp_cnt_4w) AS et_click_rate_4w,
         SAFE_DIVIDE(b.sum_et_open_cnt_4w, b.sum_et_camp_cnt_4w) AS et_open_rate_4w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_4w, b.sum_et_camp_cnt_4w) AS et_redeem_rate_4w,
         SAFE_DIVIDE(b.sum_et_click_cnt_4w, b.sum_et_open_cnt_4w) AS et_click_through_rate_4w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_4w, b.sum_et_activate_cnt_4w) AS et_conversion_rate_4w,
         -- 2 week features
         SAFE_DIVIDE(b.sum_et_activate_cnt_2w, b.sum_et_camp_cnt_2w) AS et_activate_rate_2w,
         SAFE_DIVIDE(b.sum_et_click_cnt_2w, b.sum_et_camp_cnt_2w) AS et_click_rate_2w,
         SAFE_DIVIDE(b.sum_et_open_cnt_2w, b.sum_et_camp_cnt_2w) AS et_open_rate_2w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_2w, b.sum_et_camp_cnt_2w) AS et_redeem_rate_2w,
         SAFE_DIVIDE(b.sum_et_click_cnt_2w, b.sum_et_open_cnt_2w) AS et_click_through_rate_2w,
         SAFE_DIVIDE(b.sum_et_redeem_cnt_2w, b.sum_et_activate_cnt_2w) AS et_conversion_rate_2w
  FROM   base_calc AS b
)
;

------------------------------------------------------------------------------------------------
-- 2.0 Build past transactional history features
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_txn_features_202204` AS (
  WITH txn_history AS (
    SELECT lcd.CustomerRegistrationNumber AS crn,
           ass.TXNStartDate AS start_txn_date,
           dd.FiscalWeekEndDate AS fw_end_date,
           ass.BasketKey AS basket_key,
           ass.TotalAmountIncldTax AS tot_amt_incld_gst,
           COALESCE(ass.TotalAmountIncldTax, 0) - COALESCE(ass.TotalWOWDollarIncldTax, 0) AS tot_amt,
           COALESCE(ass.TotalOfferDiscountIncldTax, 0) + COALESCE(ass.TotalStaffDiscountIncldTax, 0) + COALESCE(ass.TotalUnknownDiscountIncldTax, 0) AS tot_dscnt
    FROM   `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v` AS lcd

    INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v` AS ass
      ON lcd.LoyaltyCardNumber = ass.LoyaltyCardNumber
        AND ass.TXNStartDate >= '2019-07-01'
        AND ass.VoidFlag <> 'Y'
        AND ass.SalesOrg IN (1005, 1030)
        AND ass.TotalAmountIncldTax > 0

    INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_date_v`  AS dd
      ON ass.TXNStartDate = dd.CalendarDay

    INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_masterdata_view.dim_article_master_v` AS am
      ON am.SalesOrg = ass.SalesOrg
        AND am.ArticleWithUOM = ass.ArticleWithUOM
        AND am.DepartmentWithoutCompanyCode <> '20' -- liquor
        AND am.MerchandiseCategoryLevel2Code <> 40801 -- tobacco
        AND am.MerchandiseCategoryLevel4Code NOT IN (208030301, 208030201) -- gift cards
  ),

  base AS (
    -- base audience (expecting crn, ref_dt)
    SELECT crn,
           ref_dt,
           ref_dt + 7 AS lw_ref_dt,
           ref_dt - 7 * 3 + 1 AS l4w_dt,
           ref_dt - 7 * 1 + 1 AS l2w_dt
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
  ),

  base_long AS (
    -- prepare boolean logic / flags to simplify calculations
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           CASE
             WHEN th.start_txn_date BETWEEN b.l4w_dt AND b.lw_ref_dt
             THEN 1 ELSE 0
             END AS is_4w,
           CASE
             WHEN th.start_txn_date BETWEEN b.l2w_dt AND b.lw_ref_dt
             THEN 1 ELSE 0
             END AS is_2w,
           th.start_txn_date,
           th.fw_end_date,
           th.basket_key,
           th.tot_amt_incld_gst,
           th.tot_amt,
           th.tot_dscnt
    FROM   base AS b

    -- transaction history
    LEFT JOIN txn_history AS th
      ON b.crn = th.crn
        AND th.start_txn_date BETWEEN b.l4w_dt AND b.lw_ref_dt
  ),

  base_weekly AS (
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           b.fw_end_date,
           NULLIF(SUM(b.tot_amt * b.is_4w), 0) AS weekly_tot_amt_4w,
           NULLIF(SUM(b.tot_amt * b.is_2w), 0) AS weekly_tot_amt_2w
    FROM   base_long AS b
    GROUP BY
      b.crn,
      b.ref_dt,
      b.lw_ref_dt,
      b.fw_end_date
  ),

  base_weekly_smry AS (
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           AVG(b.weekly_tot_amt_4w) AS avg_wkly_amt_4w,
           MIN(b.weekly_tot_amt_4w) AS min_wkly_amt_4w,
           MAX(b.weekly_tot_amt_4w) AS max_wkly_amt_4w,
           AVG(b.weekly_tot_amt_2w) AS avg_wkly_amt_2w,
           MIN(b.weekly_tot_amt_2w) AS min_wkly_amt_2w,
           MAX(b.weekly_tot_amt_2w) AS max_wkly_amt_2w
    FROM   base_weekly AS b
    GROUP BY
      b.crn,
      b.ref_dt,
      b.lw_ref_dt
  ),

  base_calc AS (
    SELECT b.crn,
           b.ref_dt,
           b.lw_ref_dt,
           -- 4 week features
           SUM(b.tot_amt * b.is_4w) AS txn_tot_amt_4w,
           SUM(b.tot_dscnt * b.is_4w) AS txn_tot_dscnt_4w,
           SAFE_DIVIDE(SUM(b.tot_dscnt * b.is_4w), SUM(b.tot_amt_incld_gst * b.is_4w)) AS txn_tot_dscnt_perc_4w,
           COUNT(DISTINCT(CASE
                            WHEN b.is_4w = 1 THEN b.basket_key
                            END)) AS txn_tot_cnt_4w,
           SAFE_DIVIDE(DATE_DIFF(MAX(CASE
                                       WHEN b.is_4w = 1 THEN b.start_txn_date
                                       END),
                                 MIN(CASE
                                       WHEN b.is_4w = 1 THEN b.start_txn_date
                                       END), DAY),
                       COUNT(DISTINCT(CASE
                                        WHEN b.is_4w = 1 THEN b.basket_key
                                        END)) - 1) AS txn_ipi_4w,
           -- 2 week features
           SUM(b.tot_amt * b.is_2w) AS txn_tot_amt_2w,
           SUM(b.tot_dscnt * b.is_2w) AS txn_tot_dscnt_2w,
           SAFE_DIVIDE(SUM(b.tot_dscnt * b.is_2w), SUM(b.tot_amt_incld_gst * b.is_2w)) AS txn_tot_dscnt_perc_2w,
           COUNT(DISTINCT(CASE
                            WHEN b.is_2w = 1 THEN b.basket_key
                            END)) AS txn_tot_cnt_2w,
           SAFE_DIVIDE(DATE_DIFF(MAX(CASE
                                       WHEN b.is_2w = 1 THEN b.start_txn_date
                                       END),
                                 MIN(CASE
                                       WHEN b.is_2w = 1 THEN b.start_txn_date
                                       END), DAY),
                       COUNT(DISTINCT(CASE
                                        WHEN b.is_2w = 1 THEN b.basket_key
                                        END)) - 1) AS txn_ipi_2w
    FROM   base_long AS b
    GROUP BY
      b.crn,
      b.ref_dt,
      b.lw_ref_dt
  )

  SELECT b.*,
         ws.* EXCEPT (crn, ref_dt, lw_ref_dt)
  FROM   base_calc AS b
  LEFT JOIN base_weekly_smry AS ws
    ON b.crn = ws.crn
      AND b.ref_dt = ws.ref_dt
)
;

------------------------------------------------------------------------------------------------
-- 3.0 Evaluate the Scanner / booster segment
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_booster_status_202204` AS (
  WITH ref_dates AS (
    -- consider one more week after CMD ref_dt as ref_dt when computing custom booster features
    SELECT ref_dt,
           ref_dt + 7 AS lw_ref_dt,
           ref_dt - 7 * 7 + 1 AS l8w_dt
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
    GROUP BY
      ref_dt,
      lw_ref_dt,
      l8w_dt
  ),

  scanner_crn AS (
    SELECT ref.ref_dt,
           ref.lw_ref_dt,
           lcd.CustomerRegistrationNumber AS crn
    FROM   ref_dates AS ref

    -- scanned transactions
    INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v` AS ass
      ON ass.TXNStartDate BETWEEN ref.l8w_dt AND ref.lw_ref_dt

    -- historical cards
    INNER JOIN `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v` AS lcd
      ON ass.LoyaltyCardNumber = lcd.LoyaltyCardNumber

    GROUP BY
      ref.ref_dt,
      ref.lw_ref_dt,
      lcd.CustomerRegistrationNumber
  ),

  booster_crn AS (
    SELECT DISTINCT
           ref.ref_dt,
           ref.lw_ref_dt,
           crn
    FROM   ref_dates AS ref

    -- customer who scans the rewards card when shopping and also boosts the offers
    INNER JOIN `gcp-wow-rwds-ai-data-prod.loyalty_bi_analytics.booster_hist` AS b -- table refreshed in RedShift every Sunday
      ON ref.lw_ref_dt = b.fw_end_date
  )

  SELECT a.crn,
         a.ref_dt,
         s.lw_ref_dt,
         CASE
           WHEN b.crn IS NOT NULL THEN 'booster'
           WHEN s.crn IS NOT NULL THEN 'scanner non booster'
           ELSE 'other'
           END AS scanner_booster_segment
  FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE` AS a

  LEFT JOIN scanner_crn AS s
    ON a.crn = s.crn
      AND a.ref_dt + 7 = s.lw_ref_dt

  LEFT JOIN booster_crn AS b
    ON a.ref_dt + 7 = b.lw_ref_dt
      AND a.crn = b.crn

  GROUP BY
    a.crn,
    a.ref_dt,
    s.lw_ref_dt,
    scanner_booster_segment
)
;

------------------------------------------------------------------------------------------------
-- 4.0 Build non-email channel features
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` (
  crn                        STRING,
  ref_dt                     DATE,
  campaign_start_date        DATE,

  num_offers_cvm_email_l4w   INT64,
  num_offers_cvm_rapp_l4w    INT64,
  num_offers_cvm_hub_l4w     INT64,
  num_offers_cvm_doc_l4w     INT64,
  num_offers_cvm_fb_l4w      INT64,
  num_offers_cvm_social_l4w  INT64,
  num_offers_cvm_dm_l4w      INT64,
  num_offers_cvm_dd_l4w      INT64,

  num_offers_cvm_email_l8w   INT64,
  num_offers_cvm_rapp_l8w    INT64,
  num_offers_cvm_hub_l8w     INT64,
  num_offers_cvm_doc_l8w     INT64,
  num_offers_cvm_fb_l8w      INT64,
  num_offers_cvm_social_l8w  INT64,
  num_offers_cvm_dm_l8w      INT64,
  num_offers_cvm_dd_l8w      INT64,

  num_offers_supers_email_l4w    INT64,
  num_offers_supers_rapp_l4w     INT64,
  num_offers_supers_hub_l4w      INT64,
  num_offers_supers_doc_l4w      INT64,
  num_offers_supers_fb_l4w       INT64,
  num_offers_supers_social_l4w   INT64,
  num_offers_supers_dm_l4w       INT64,
  num_offers_supers_dd_l4w       INT64,

  num_offers_supers_email_l8w    INT64,
  num_offers_supers_rapp_l8w     INT64,
  num_offers_supers_hub_l8w      INT64,
  num_offers_supers_doc_l8w      INT64,
  num_offers_supers_fb_l8w       INT64,
  num_offers_supers_social_l8w   INT64,
  num_offers_supers_dm_l8w       INT64,
  num_offers_supers_dd_l8w       INT64,

  rapp_num_visits_l4w            INT64,
  rapp_num_visits_l8w            INT64,

  rapp_num_push_notifications_cvm_l4w        INT64,
  rapp_num_push_notifications_cvm_l8w        INT64,
  rapp_num_push_notifications_supers_l4w     INT64,
  rapp_num_push_notifications_supers_l8w     INT64,

  num_offers_et_resp_cvm_l4w     INT64,
  num_offers_et_resp_cvm_l8w     INT64,
  num_offers_et_resp_supers_l4w  INT64,
  num_offers_et_resp_supers_l8w  INT64
)
PARTITION BY ref_dt
;

INSERT INTO `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` (
  crn,
  ref_dt,
  campaign_start_date
)
SELECT crn,
       ref_dt,
       campaign_start_date
FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
GROUP BY
  crn,
  ref_dt,
  campaign_start_date
;


/* Num offers received by channel */
UPDATE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS upd
SET num_offers_cvm_email_l4w = x.num_offers_cvm_email_l4w,
    num_offers_cvm_rapp_l4w = x.num_offers_cvm_rapp_l4w,
    num_offers_cvm_hub_l4w = x.num_offers_cvm_hub_l4w,
    num_offers_cvm_doc_l4w = x.num_offers_cvm_doc_l4w,
    num_offers_cvm_fb_l4w = x.num_offers_cvm_fb_l4w,
    num_offers_cvm_social_l4w = x.num_offers_cvm_social_l4w,
    num_offers_cvm_dm_l4w = x.num_offers_cvm_dm_l4w,
    num_offers_cvm_dd_l4w = x.num_offers_cvm_dd_l4w,

    num_offers_cvm_email_l8w = x.num_offers_cvm_email_l8w,
    num_offers_cvm_rapp_l8w = x.num_offers_cvm_rapp_l8w,
    num_offers_cvm_hub_l8w = x.num_offers_cvm_hub_l8w,
    num_offers_cvm_doc_l8w = x.num_offers_cvm_doc_l8w,
    num_offers_cvm_fb_l8w = x.num_offers_cvm_fb_l8w,
    num_offers_cvm_social_l8w = x.num_offers_cvm_social_l8w,
    num_offers_cvm_dm_l8w = x.num_offers_cvm_dm_l8w,
    num_offers_cvm_dd_l8w = x.num_offers_cvm_dd_l8w,

    num_offers_supers_email_l4w = x.num_offers_supers_email_l4w,
    num_offers_supers_rapp_l4w = x.num_offers_supers_rapp_l4w,
    num_offers_supers_hub_l4w = x.num_offers_supers_hub_l4w,
    num_offers_supers_doc_l4w = x.num_offers_supers_doc_l4w,
    num_offers_supers_fb_l4w = x.num_offers_supers_fb_l4w,
    num_offers_supers_social_l4w = x.num_offers_supers_social_l4w,
    num_offers_supers_dm_l4w = x.num_offers_supers_dm_l4w,
    num_offers_supers_dd_l4w = x.num_offers_supers_dd_l4w,

    num_offers_supers_email_l8w = x.num_offers_supers_email_l8w,
    num_offers_supers_rapp_l8w = x.num_offers_supers_rapp_l8w,
    num_offers_supers_hub_l8w = x.num_offers_supers_hub_l8w,
    num_offers_supers_doc_l8w = x.num_offers_supers_doc_l8w,
    num_offers_supers_fb_l8w = x.num_offers_supers_fb_l8w,
    num_offers_supers_social_l8w = x.num_offers_supers_social_l8w,
    num_offers_supers_dm_l8w = x.num_offers_supers_dm_l8w,
    num_offers_supers_dd_l8w = x.num_offers_supers_dd_l8w
FROM (
  WITH cal AS (
    SELECT crn,
           campaign_or_control_start_date AS campaign_start_date,
           campaign_or_control_end_date AS campaign_send_date,
           campaign_code,
           LOWER(channel_type) AS channel_type
    FROM   `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.ca_campaigns_audience_list_view`
    WHERE  IFNULL(target_control, 'T') = 'T'
      AND LEFT(campaign_code, 3) IN ('CAT', 'CNA', 'CVM', 'ENG', 'LCP', 'NCP', 'OSP', 'ONL', 'ECM')
    GROUP BY
      crn,
      campaign_start_date,
      campaign_send_date,
      campaign_code,
      channel_type
  ),

  base_flags AS (
    SELECT bse.crn,
           bse.ref_dt,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(edm|email)')
             THEN 1 ELSE 0
             END AS is_email,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(rapp|rewardsapp|)')
             THEN 1 ELSE 0
             END AS is_rapp,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(hub|rhb|rh)')
             THEN 1 ELSE 0
             END AS is_hub,
           CASE
             WHEN cal.channel_type IN ('doc', 'dckt')
             THEN 1 ELSE 0
             END AS is_doc,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(fb|facebook)')
             THEN 1 ELSE 0
             END AS is_fb,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(soc|social)')
             THEN 1 ELSE 0
             END AS is_social,
           CASE
             WHEN cal.channel_type IN ('dm')
             THEN 1 ELSE 0
             END AS is_dm,
           CASE
             WHEN REGEXP_CONTAINS(cal.channel_type, '(dd)')
             THEN 1 ELSE 0
             END AS is_dd,
           CASE
             WHEN LEFT(cal.campaign_code, 3) IN ('CVM')
             THEN 1 ELSE 0
             END AS is_cvm,
           CASE
             WHEN DATE_SUB(bse.ref_dt, INTERVAL 4 week) <= cal.campaign_send_date
               AND cal.campaign_start_date <= bse.ref_dt
               THEN 1 ELSE 0
             END AS is_l4w
    FROM   `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS bse
    LEFT JOIN cal
        ON cal.crn = bse.crn
          AND DATE_SUB(bse.ref_dt, INTERVAL 8 week) <= cal.campaign_send_date
          AND cal.campaign_start_date <= bse.ref_dt
  )

  SELECT crn,
         ref_dt,

         /* cvm l4w */
         SUM(CASE
               WHEN is_cvm = 1 AND is_email = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_email_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_rapp = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_rapp_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_hub = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_hub_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_doc = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_doc_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_fb = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_fb_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_social = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_social_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_dm = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_dm_l4w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_dd = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_dd_l4w,

         /* cvm l8w */
         SUM(CASE
               WHEN is_cvm = 1 AND is_email = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_email_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_rapp = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_rapp_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_hub = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_hub_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_doc = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_doc_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_fb = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_fb_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_social = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_social_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_dm = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_dm_l8w,
         SUM(CASE
               WHEN is_cvm = 1 AND is_dd = 1
               THEN 1 ELSE 0
               END) AS num_offers_cvm_dd_l8w,

         /* supers l4w */
         SUM(CASE
               WHEN is_email = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_email_l4w,
         SUM(CASE
               WHEN is_rapp = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_rapp_l4w,
         SUM(CASE
               WHEN is_hub = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_hub_l4w,
         SUM(CASE
               WHEN is_doc = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_doc_l4w,
         SUM(CASE
               WHEN is_fb = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_fb_l4w,
         SUM(CASE
               WHEN is_social = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_social_l4w,
         SUM(CASE
               WHEN is_dm = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_dm_l4w,
         SUM(CASE
               WHEN is_dd = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_dd_l4w,

         /* supers l8w */
         SUM(CASE
               WHEN is_email = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_email_l8w,
         SUM(CASE
               WHEN is_rapp = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_rapp_l8w,
         SUM(CASE
               WHEN is_hub = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_hub_l8w,
         SUM(CASE
               WHEN is_doc = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_doc_l8w,
         SUM(CASE
               WHEN is_fb = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_fb_l8w,
         SUM(CASE
               WHEN is_social = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_social_l8w,
         SUM(CASE
               WHEN is_dm = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_dm_l8w,
         SUM(CASE
               WHEN is_dd = 1
               THEN 1 ELSE 0
               END) AS num_offers_supers_dd_l8w
  FROM   base_flags AS bf
  GROUP BY
    crn,
    ref_dt
) AS x
WHERE x.crn = upd.crn
  AND x.ref_dt = upd.ref_dt
;

/* Rewards app visits */
UPDATE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS upd
SET rapp_num_visits_l4w = x.rapp_num_visits_l4w,
    rapp_num_visits_l8w = x.rapp_num_visits_l8w
FROM (
  SELECT bse.crn,
         bse.ref_dt,
         SUM(CASE
               WHEN act.clndr_date BETWEEN DATE_SUB(bse.ref_dt, INTERVAL 4 week) AND bse.ref_dt
               THEN 1 ELSE 0
               END) AS rapp_num_visits_l4w,
         SUM(CASE
               WHEN act.crn IS NOT NULL
               THEN 1 ELSE 0
               END) AS rapp_num_visits_l8w
  FROM   `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS bse
  LEFT JOIN `gcp-wow-rwds-ai-data-prod.loyalty_bi_analytics.rewards_app_crn_active_in_app` AS act
    ON act.crn = bse.crn
      AND act.clndr_date BETWEEN DATE_SUB(bse.ref_dt, INTERVAL 8 week)
      AND bse.ref_dt
  GROUP BY
    bse.crn,
    bse.ref_dt
) AS x
WHERE x.crn = upd.crn
  AND x.ref_dt = upd.ref_dt
;

/* Push notifications */
UPDATE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS upd
SET rapp_num_push_notifications_cvm_l4w = x.rapp_num_push_notifications_cvm_l4w,
    rapp_num_push_notifications_cvm_l8w = x.rapp_num_push_notifications_cvm_l8w,
    rapp_num_push_notifications_supers_l4w = x.rapp_num_push_notifications_supers_l4w,
    rapp_num_push_notifications_supers_l8w = x.rapp_num_push_notifications_supers_l8w
FROM (
  WITH pn AS (
    SELECT crn,
           CASE
             WHEN LEFT(campaign_code, 3) = 'CVM'
             THEN 1 ELSE 0
             END AS is_cvm,
           DATE(client_time) AS client_time
    FROM   `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.rewards_app_push_notifications_all_sends_view`
    WHERE LEFT(campaign_code, 3) IN ('CAT', 'CNA', 'CVM', 'ENG', 'LCP', 'NCP', 'OSP', 'ONL', 'ECM')
  ),

  base_flags AS (
    SELECT bse.crn,
           bse.ref_dt,
           pn.is_cvm,
           CASE
             WHEN pn.client_time BETWEEN DATE_SUB(bse.ref_dt, INTERVAL 4 week) AND bse.ref_dt
             THEN 1 ELSE 0
             END AS is_l4w,
           CASE
             WHEN pn.crn IS NOT NULL
             THEN 1 ELSE 0
             END AS is_pn
    FROM `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS bse
    LEFT JOIN pn
      ON pn.crn = bse.crn
        AND pn.client_time BETWEEN DATE_SUB(bse.ref_dt, INTERVAL 8 week)
        AND bse.ref_dt
  )

  SELECT crn,
         ref_dt,
         SUM(CASE
               WHEN is_cvm = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS rapp_num_push_notifications_cvm_l4w,
         SUM(CASE
               WHEN is_cvm = 1
               THEN 1 ELSE 0
               END) AS rapp_num_push_notifications_cvm_l8w,
         SUM(CASE
               WHEN is_l4w = 1
               THEN 1 ELSE 0
               END) AS rapp_num_push_notifications_supers_l4w,
         SUM(is_pn) AS rapp_num_push_notifications_supers_l8w
    FROM base_flags
    GROUP BY
      crn,
      ref_dt
  ) AS x
WHERE x.crn = upd.crn
  AND x.ref_dt = upd.ref_dt
;

/* Email offers: et_resp */
UPDATE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS upd
SET num_offers_et_resp_cvm_l4w = x.num_offers_et_resp_cvm_l4w,
    num_offers_et_resp_cvm_l8w = x.num_offers_et_resp_cvm_l8w,
    num_offers_et_resp_supers_l4w = x.num_offers_et_resp_supers_l4w,
    num_offers_et_resp_supers_l8w = x.num_offers_et_resp_supers_l8w
FROM (
  WITH rsl AS (
    SELECT CustomerRegistrationNumber AS crn,
           DATE(OfferExecutionStartDate) AS offer_exec_start_date,
           DATE(OfferExecutionEndDate) AS offer_exec_end_date,
           CASE
             WHEN LEFT(CampaignCode, 3) = 'CVM'
             THEN 1 ELSE 0
             END AS is_cvm
    FROM    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_campaign_view.et_resp_sendlog_v`
    WHERE  LEFT(CampaignCode, 3) IN ('CAT', 'CNA', 'CVM', 'ENG', 'LCP', 'NCP', 'OSP', 'ONL', 'ECM')
    GROUP BY
      crn,
      offer_exec_start_date,
      offer_exec_end_date,
      is_cvm
  ),

  base_flags AS (
    SELECT bse.crn,
           bse.ref_dt,
           rsl.is_cvm,
           CASE
             WHEN DATE_SUB(bse.ref_dt, INTERVAL 4 week) <= rsl.offer_exec_end_date
               AND rsl.offer_exec_start_date <= bse.ref_dt
             THEN 1 ELSE 0
             END AS is_l4w,
           CASE
             WHEN rsl.crn IS NOT NULL
             THEN 1 ELSE 0
             END AS is_email
    FROM   `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS bse
    LEFT JOIN rsl
      ON rsl.crn = bse.crn
        AND DATE_SUB(bse.ref_dt, INTERVAL 8 week) <= rsl.offer_exec_end_date
        AND rsl.offer_exec_start_date <= bse.ref_dt
  )

  SELECT crn,
         ref_dt,
         SUM(CASE
               WHEN is_cvm = 1 AND is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_et_resp_cvm_l4w,
         SUM(CASE
               WHEN is_cvm = 1
               THEN 1 ELSE 0
               END) AS num_offers_et_resp_cvm_l8w,
         SUM(CASE
               WHEN is_l4w = 1
               THEN 1 ELSE 0
               END) AS num_offers_et_resp_supers_l4w,
         SUM(is_email) AS num_offers_et_resp_supers_l8w
  FROM   base_flags
  GROUP BY
    crn,
    ref_dt
  ) AS x
WHERE x.crn = upd.crn
  AND x.ref_dt = upd.ref_dt
;

------------------------------------------------------------------------------------------------
-- 5.0 Evaluate seasonality / relative holiday / event features
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_HOLIDAY_FEATURES_202204` AS

WITH base_date AS (
  SELECT DATE(campaign_start_date) AS campaign_start_date,
         ref_dt
  FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
  GROUP BY
    campaign_start_date,
    ref_dt
),

-- extract next national holiday and observance information
next_holiday AS (
  SELECT * EXCEPT(rn)
  FROM   (
    SELECT bd.ref_dt,
           bd.campaign_start_date,
           ph.holiday_name,
           ph.doy,
           ph.woy,
           ph.dow,
           ph.holiday_type,
           DATE(ph.date) AS holiday_dt,
           ROW_NUMBER() OVER(PARTITION BY ph.holiday_type, bd.campaign_start_date  ORDER BY DATE(ph.date)) AS rn
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.public_holidays` AS ph
    INNER JOIN base_date AS bd
      ON DATE(bd.campaign_start_date) <= DATE(DATE)
  )
  WHERE rn = 1
),

-- extract the next marketing event related info
marketing_event AS (
  SELECT * EXCEPT(rn)
  FROM   (
    SELECT me.*,
           bd.ref_dt,
           bd.campaign_start_date,
           ROW_NUMBER() OVER(PARTITION BY bd.campaign_start_date ORDER BY DATE(me.Start_date)) AS rn
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.marketing_events` AS me
    INNER JOIN base_date AS bd
      ON bd.campaign_start_date <= me.End_date
    WHERE  me.ATL_campaigns = 1
  )
  WHERE rn = 1
),

-- extract the last ATL event info
last_ATL_event AS (
  SELECT * EXCEPT(rn) 
  FROM   (
    SELECT me.*,
           bd.ref_dt,
           bd.campaign_start_date,
           ROW_NUMBER() OVER(PARTITION BY bd.campaign_start_date ORDER BY DATE(me.Start_date) DESC) AS rn
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.marketing_events` AS me
    INNER JOIN base_date AS bd
      ON bd.campaign_start_date >= me.End_date
    WHERE me.ATL_campaigns = 1
  )
  WHERE rn = 1
),

base_Easter AS (
  SELECT DATE(date) AS Easter,
         ref_dt,
         campaign_start_date
  FROM   (
    SELECT a.date,
           b.ref_dt,
           b.campaign_start_date,
           ROW_NUMBER() OVER(PARTITION BY a.holiday_type, b.campaign_start_date ORDER BY DATE(a.date)) AS rn
    FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.public_holidays` AS a
    INNER JOIN base_date AS b
      ON b.campaign_start_date <= DATE(DATE)
    WHERE  REGEXP_CONTAINS(a.holiday_name, 'Good Friday'))
  WHERE rn = 1
)

SELECT -- base date information, current week of year, day of year
       bd.ref_dt,
       bd.campaign_start_date,
       FORMAT_DATE("%B", bd.campaign_start_date) AS moy,
       EXTRACT(WEEK FROM bd.campaign_start_date) AS woy,
       EXTRACT(DAYOFYEAR FROM bd.campaign_start_date) AS doy,
       FORMAT_DATE('%A', bd.campaign_start_date) AS wday,
       -- next national holiday info
       nh.holiday_name AS next_nh,
       DATE_DIFF(nh.holiday_dt, nh.campaign_start_date, DAY) AS days_to_nnh,
       nh.doy AS doy_nnh,
       nh.woy AS woy_nnh,
       FORMAT_DATE('%A', nh.holiday_dt) AS dow_nhh,
       FORMAT_DATE("%B", nh.holiday_dt) AS moy_nhh,
       -- next observance info
       nod.holiday_name AS next_od,
       DATE_DIFF(nod.holiday_dt, nh.campaign_start_date, DAY) AS days_to_nod,
       nod.doy AS doy_nod,
       nod.woy AS woy_nod,
       FORMAT_DATE('%A', nod.holiday_dt) AS dow_nod,
       FORMAT_DATE("%B", nod.holiday_dt) AS moy_nod,
       -- days to Christmas and Easter
       DATE_DIFF(DATE(EXTRACT(YEAR FROM bd.campaign_start_date), 12, 25), bd.campaign_start_date, DAY) AS days_to_Xmas,
       DATE_DIFF(be.Easter, bd.campaign_start_date, DAY) AS days_to_Easter,
       -- ATL event info
       CASE
         WHEN bd.campaign_start_date BETWEEN me.Start_date AND me.End_date THEN TRUE
         ELSE FALSE
         END AS ATL_event,
       CASE
         WHEN REGEXP_CONTAINS(me.Event_Name, r'(?i)collectables') AND REGEXP_CONTAINS(me.Event_Name, r'(?i)ooshies') THEN 'collectable_ooshies'
         WHEN REGEXP_CONTAINS(me.Event_Name, r'(?i)collectables') THEN 'collectable_others'
         ELSE 'ATL_event_others'
         END AS ATL_type,
       CASE
         WHEN bd.campaign_start_date BETWEEN me.Start_date AND me.End_date THEN DATE_DIFF(bd.campaign_start_date, me.Start_date, WEEK)
         ELSE 0
         END AS wks_curr_ATL,
       DATE_DIFF(bd.campaign_start_date, le.End_date, WEEK) AS wks_last_ATL
FROM   base_date AS bd

LEFT JOIN next_holiday AS nh
  ON bd.ref_dt = nh.ref_dt
    AND  nh.holiday_type = 'National Holiday'

LEFT JOIN next_holiday AS nod
  ON bd.ref_dt = nod.ref_dt
    AND  nod.holiday_type = 'Observance'

LEFT JOIN base_Easter AS be
  ON bd.ref_dt = be.ref_dt

LEFT JOIN marketing_event AS me
  ON me.ref_dt = bd.ref_dt

LEFT JOIN last_ATL_event AS le
  ON le.ref_dt = bd.ref_dt

ORDER BY ref_dt
;

------------------------------------------------------------------------------------------------
-- 6.0 Extract last MMM execution features (what campaign customer received, response)
------------------------------------------------------------------------------------------------
DROP TABLE IF EXISTS `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FEATURES_FROM_PAST_EXECUTIONS_202204`;
CREATE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FEATURES_FROM_PAST_EXECUTIONS_202204` AS
    WITH crn_base AS (
        SELECT  crn,
                ref_dt,
                DATE(campaign_start_date) AS campaign_start_date
        FROM   `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
        GROUP BY
        crn,
        ref_dt,
        campaign_start_date
    ),
    mmm_campaign_codes AS (
        --MMM Campaign Codes used AS a filter
        SELECT  DISTINCT campaign_code
        FROM    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
    ),
    test_cards AS (
        -- Test cards to be removed to bring data to CRN level
        SELECT  DISTINCT crn
        FROM    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_TEST_CARDS`
        UNION ALL
        -- legacy test cards
        SELECT '3300000000002737797'
    ),
    unique_dates AS (
        SELECT  campaign_start_date,
                ROW_NUMBER() OVER (ORDER BY campaign_start_date DESC) AS row_no
        FROM    (
            SELECT  DISTINCT DATE(campaign_start_date) AS campaign_start_date
            FROM    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER`
        )
    ),
    previous_execution_dates AS (
        SELECT  curr_exec.campaign_start_date,
                prev1_exec.campaign_start_date AS last1_execution_date,
                prev2_exec.campaign_start_date AS last2_execution_date,
                prev3_exec.campaign_start_date AS last3_execution_date
        FROM    unique_dates AS curr_exec
    
    LEFT JOIN unique_dates AS prev1_exec
      ON curr_exec.row_no + 1 = prev1_exec.row_no

    LEFT JOIN unique_dates AS prev2_exec
      ON curr_exec.row_no + 2 = prev2_exec.row_no
      
    LEFT JOIN unique_dates AS prev3_exec
      ON curr_exec.row_no + 3 = prev3_exec.row_no
    ),
    --MMM Campaign Performance across each execution 
    campaign_performance AS (
        SELECT  mam1.campaign_code,
                CASE --@todo: handle past rewards, hurdle features mp, tiered mp
                    WHEN mam1.campaign_type IN ('MP','TMP') THEN NULL
                    ELSE mam1.spend_hurdle
                    END AS spend_hurdle,
                CASE --@todo: handle past rewards, hurdle features mp, tiered mp
                    WHEN mam1.campaign_type IN ('MP','TMP') THEN NULL
                    ELSE mam1.total_rewards
                    END AS total_rewards,
                mam1.crn,
                mam1.campaign_start_date,
                fcs1.activate_flag,
                fcs1.redeem_flag,
                fcs1.purchase_flag
        FROM (
            SELECT  crn,
                    DATE(campaign_start_date) AS campaign_start_date,
                    campaign_code,
                    spend_hurdle,
                    campaign_type,
                    total_wow_rewards AS total_rewards
            FROM    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_AUDIENCE_MASTER` AS am
            WHERE crn NOT IN (
                SELECT  crn
                FROM    test_cards
            )
        ) AS mam1
        
        LEFT JOIN (
            SELECT  fcs.crn,
                    mcc.campaign_code,
                    fcs.campaign_start_date, 
                    MAX(CASE WHEN fcs.activate_flag IS NULL THEN 0 ELSE 1 END) AS activate_flag,
                    MAX(CASE WHEN fcs.redeem_flag IS NULL THEN 0 ELSE 1 END) AS redeem_flag,
                    MAX(CASE WHEN fcs.tot_amt_incld_gst_promo > 0 THEN 1 ELSE 0 END) AS purchase_flag
            FROM    mmm_campaign_codes AS mcc

            -- filter for only relevant records of fcs
            INNER JOIN `gcp-wow-rwds-ai-data-prod.loyalty_car_analytics.fact_campaign_sales_view` AS fcs
                ON mcc.campaign_code = LEFT(fcs.campaign_code,8)

            GROUP BY fcs.crn, mcc.campaign_code, fcs.campaign_start_date
        ) AS fcs1
            ON mam1.crn = fcs1.crn
            AND mam1.campaign_code = fcs1.campaign_code
            AND mam1.campaign_start_date = fcs1.campaign_start_date
    )
    
    SELECT  --base
            base.crn,
            base.ref_dt,
            base.campaign_start_date,
            --l1 week
            w1.campaign_start_date AS last1_execution_date,
            DATE_DIFF(base.campaign_start_date,w1.campaign_start_date, DAY) AS days_past_l1w_campaign,
            w1.campaign_code AS l1w_campaign_code,
            w1.spend_hurdle AS l1w_spend_hurdle,
            w1.total_rewards AS l1w_total_rewards,
            w1.activate_flag AS l1w_activate_flag,
            w1.redeem_flag AS l1w_redeem_flag,
            w1.purchase_flag AS l1w_purchase_flag,
            --l2 week
            w2.campaign_start_date AS last2_execution_date,
            DATE_DIFF(base.campaign_start_date,w2.campaign_start_date, DAY) AS days_past_l2w_campaign,
            w2.campaign_code AS l2w_campaign_code,
            w2.spend_hurdle AS l2w_spend_hurdle,
            w2.total_rewards AS l2w_total_rewards,
            w2.activate_flag AS l2w_activate_flag,
            w2.redeem_flag AS l2w_redeem_flag,
            w2.purchase_flag AS l2w_purchase_flag,
            --l3 week
            w3.campaign_start_date AS last3_execution_date,
            DATE_DIFF(base.campaign_start_date,w3.campaign_start_date, DAY) AS days_past_l3w_campaign,
            w3.campaign_code AS l3w_campaign_code,
            w3.spend_hurdle AS l3w_spend_hurdle,
            w3.total_rewards AS l3w_total_rewards,
            w3.activate_flag AS l3w_activate_flag,
            w3.redeem_flag AS l3w_redeem_flag,
            w3.purchase_flag AS l3w_purchase_flag

    FROM    crn_base AS base

    -- previous execution dates for each ref_dt
    LEFT JOIN previous_execution_dates AS ped
        ON base.campaign_start_date = ped.campaign_start_date
    
    -- corresponding campaign performance (last execution)
    LEFT JOIN campaign_performance AS w1
        ON base.crn = w1.crn 
        AND ped.last1_execution_date = w1.campaign_start_date

    -- corresponding campaign performance (2nd last execution)
    LEFT JOIN campaign_performance AS w2
        ON base.crn = w2.crn 
        AND ped.last2_execution_date = w2.campaign_start_date

    -- corresponding campaign performance (3rd last execution)
    LEFT JOIN campaign_performance AS w3
        ON base.crn = w3.crn 
        AND ped.last3_execution_date = w3.campaign_start_date
;



-- ------------------------------------------------------------------------------------------------
-- -- 7.0 Last 18 week average spend - crn, campaign_start_date level, Campaign_Seg_Grp_Id
-- ------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE
  `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_avg18wk_per_basket_202204` AS (
  SELECT
    lcd.CustomerRegistrationNumber AS crn,
    cam.Campaign_Seg_Grp_Id,
    pbma.ref_dt,
    ROUND(SUM(ass.TotalAmountIncldTax - coalesce(ass.TotalWOWDollarIncldTax,
          0)) / COUNT(DISTINCT(ass.BasketKey)),2) AS avg18wk_per_basket
  FROM
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_customer_view.loyalty_card_detail_v` AS lcd -- link TO scanned transactions
  INNER JOIN
    `gcp-wow-ent-im-wowx-cust-prod.adp_wowx_dm_integrated_sales_view.article_sales_summary_v` AS ass
  ON
    lcd.LoyaltyCardNumber = ass.LoyaltyCardNumber -- apply inclusion FOR category specific products
  INNER JOIN (
    SELECT
      crn,
      ref_dt,
      Campaign_Seg_Grp_Id
    FROM
      `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
    GROUP BY
      crn,
      ref_dt,
      Campaign_Seg_Grp_Id ) AS pbma
  ON
    lcd.CustomerRegistrationNumber = pbma.crn
  INNER JOIN (
    SELECT
      Campaign_Seg_Grp_Id,
      prod_nbr
    FROM
      `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_CAT_ARTICLE_MASTER`
    WHERE
      Offer_Selection_Flag = 'Y'
      AND Audience_Selection_Flag = 'Y'
      AND Campaign_Start_Date = (
      SELECT
        MAX(Campaign_Start_Date)
      FROM
        `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_CAT_ARTICLE_MASTER` )
    GROUP BY
      Campaign_Seg_Grp_Id,
      prod_nbr ) AS cam
  ON
    ass.ArticleWithUOM = cam.prod_nbr
  WHERE
    ass.TXNStartDate BETWEEN DATE_TRUNC(DATE_ADD(pbma.ref_dt, INTERVAL -1 day), WEEK(Tuesday)) - (7 * 18 - 1)
    AND DATE_TRUNC(DATE_ADD(pbma.ref_dt, INTERVAL -1 day), WEEK(Tuesday))
    AND CAST(ass.VoidFlag AS String) != 'Y'
    AND ass.SalesOrg IN (1005,
      1030)
    AND ass.ProductQty > 0
    AND (ass.TotalAmountIncldTax - coalesce(ass.TotalWOWDollarIncldTax,
        0)) > 0
    AND cam.Campaign_Seg_Grp_Id = pbma.Campaign_Seg_Grp_Id
  GROUP BY
    lcd.CustomerRegistrationNumber,
    cam.Campaign_Seg_Grp_Id,
    pbma.ref_dt)
;
------------------------------------------------------------------------------------------------
-- 8.0 Output table - crn, campaign_start_date level
------------------------------------------------------------------------------------------------
CREATE OR REPLACE TABLE `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FINAL_FEATURES_CAMP` AS
WITH post_bq_smry AS (
  SELECT crn,
         ref_dt,
         offer_type,
         Model,
         campaign_code,
         Campaign_Seg_Grp_Id,
         campaign_length,
         campaign_start_date,
         Template_id,
         partition_date,
         AVG_SPEND_BAND_L,
         AVG_SPEND_BAND_H,
         MAX(spend_hurdle) AS spend_hurdle, 
         MAX(spend_type) AS spend_type,
         MAX(rewards) AS rewards,
         AVG(weekly_inc_sales) AS weekly_inc_sales,
         AVG(weekly_spd) AS weekly_spd,
         MAX(email_mktb_flag) AS email_mktb_flag,
         MAX(redeem_flag) AS redeem_flag,
         SUM(COALESCE(as_weekly_spd, 0)) / MAX(campaign_week_nbr) AS as_wkly_spd_avg,
         SUM(COALESCE(as_weekly_spd, 0)) AS as_wkly_spd_sum
  FROM    `gcp-wow-rwds-ai-mmm-prod.PROD_MMM_CAT.MMM_POST_BQ_MASTER_AUDIENCE`
  GROUP BY
    crn,
    ref_dt,
    offer_type,
    Model,
    campaign_code,
    Campaign_Seg_Grp_Id,
    campaign_length,
    campaign_start_date,
    Template_id,
    partition_date,
    AVG_SPEND_BAND_L,
    AVG_SPEND_BAND_H
)

SELECT pbq.*,
       chf.*   EXCEPT (crn, ref_dt, lw_ref_dt),
       ctf.*   EXCEPT (crn, ref_dt, lw_ref_dt),
       cbs.scanner_booster_segment,
       nem.*   EXCEPT (crn, ref_dt, campaign_start_date),
       hf.*    EXCEPT (ref_dt, campaign_start_date),
       pe.*    EXCEPT (crn, ref_dt, campaign_start_date),
       fga.*   EXCEPT (ref_ts, crn),
       fwe.*   EXCEPT (ref_ts, crn),
       mce.*   EXCEPT (Campaign_Seg_Grp_Id, ref_dt),
       avg18wk.avg18wk_per_basket,
       CASE
          WHEN pbq.ref_dt BETWEEN covid_start_date_1 AND covid_end_date_1 
            THEN 1 - DATE_DIFF(pbq.ref_dt, covid_start_date_1, DAY) / covid_length_1
          WHEN pbq.ref_dt BETWEEN covid_start_date_2 AND covid_end_date_2 
            THEN 1 - DATE_DIFF(pbq.ref_dt, covid_start_date_2, DAY) / covid_length_2
          ELSE 0
       END AS covid_flag
       
FROM post_bq_smry AS pbq

-- campaign history features
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_campaign_hist_features_202204` AS chf
  ON chf.crn = pbq.crn
    AND chf.ref_dt = pbq.ref_dt

-- customer transaction features
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_txn_features_202204` AS ctf
  ON ctf.crn = pbq.crn
    AND ctf.ref_dt = pbq.ref_dt

-- scanner / booster segmentation
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_cust_booster_status_202204` AS cbs
  ON cbs.crn = pbq.crn
    AND cbs.ref_dt = pbq.ref_dt

-- non email channel features
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_NEM_channel_features_202204` AS nem
  ON pbq.crn = nem.crn
    AND pbq.campaign_start_date = nem.campaign_start_date

-- seasonality and holiday features
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_HOLIDAY_FEATURES_202204` AS hf
  ON pbq.campaign_start_date = hf.campaign_start_date

-- past mmm execution features
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_FEATURES_FROM_PAST_EXECUTIONS_202204` AS pe
  ON pbq.crn = pe.crn
    AND pbq.ref_dt = pe.ref_dt

-- last 18 week average spend
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_avg18wk_per_basket_202204` AS avg18wk
  ON pbq.crn = avg18wk.crn
    AND pbq.ref_dt = avg18wk.ref_dt
    AND pbq.campaign_seg_grp_id = avg18wk.campaign_seg_grp_id

-- Category embeddings
LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_category_embeddings_202204` AS mce
  ON pbq.Campaign_Seg_Grp_Id = mce.Campaign_Seg_Grp_Id
    AND pbq.ref_dt = mce.ref_dt
;