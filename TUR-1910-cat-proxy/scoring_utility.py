import os
import sys
import datetime

from google.cloud import storage
from google.cloud import bigquery
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import pandas as pd
import pickle
import tqdm

# import akl
from akl.preprocessor.categorical import PreprocessorCat
from akl.preprocessor.numeric import PreprocessorNum

import akl.lgbmfit
sys.modules['lgbmfit'] =  akl.lgbmfit

from sklearn.metrics import mean_squared_error, roc_auc_score


def load_model(gs_path_name):

    def _download(project_name, bucket_name, path_name):
        client = storage.Client(project_name)
        bucket = client.get_bucket(bucket_name)
        blob = bucket.blob(path_name)
        blob.download_to_filename('downloaded_model.pickle')

    def _unpickle(file_path):
        try:
            with open(file_path, 'rb') as f:
                obj = pickle.load(f)
                return obj
        except Exception as e:
            raise

    project_name = 'gcp-wow-rwds-ai-mmm-prod'
    bucket_name = gs_path_name[5:][:gs_path_name[5:].find('/')]
    path_name = gs_path_name[5:][gs_path_name[5:].find('/')+1:]

    _download(project_name, bucket_name, path_name)
    response = _unpickle('downloaded_model.pickle')
    os.remove('downloaded_model.pickle')
    
    return response


def adhoc_predict(model, df, output_name):
        
    def _predict(df, loaded_model, ids, output_name):
        '''Load and prepare data
        Args:
            output_name (str): Output column name
        '''

        def _get_feature_list(data, model_feature_list, model_obj):
            '''Get numeric and categorical features'''
            # get schema
            feature_list = data.columns.tolist()

            # get spec for both bum and cat
            spec_num = model_obj.config['spec_num']
            spec_cat = model_obj.config['spec_cat']

            cond = (spec_num.input != 'ignore') & (
                spec_num.new_feature.isin(model_feature_list)
            )
            spec_num_features = spec_num[cond].feature

            cond = (spec_cat.input != 'ignore') & (
                spec_cat.new_feature.isin(model_feature_list)
            )
            spec_cat_features = spec_cat[cond].feature

            num_list = [item for item in spec_num_features if item in feature_list]
            cat_list = [item for item in spec_cat_features if item in feature_list]

            return num_list, cat_list

        # get spec for both bum and cat
        spec_num = loaded_model.config['spec_num']
        spec_cat = loaded_model.config['spec_cat']

        # get list of model features
        # .values # lgbm==2.3.1 changed API from series to list
        model_feature_list = loaded_model.columns
        model_feature_list = pd.DataFrame(model_feature_list, dtype=str)[
            0
        ].apply(lambda x: x.split('||')[0])


        num_list, cat_list = _get_feature_list(
            df, model_feature_list, loaded_model
        )

        # separate in numerical and categorical
        num_df = df[num_list]
        cat_df = df[cat_list]
        # Process Numeric Attribute

        if num_df.shape[0] != 0:
            pipeline_num = PreprocessorNum(spec_num, update_spec=False)
            for col in num_df.columns:
                num_df[col] = pd.to_numeric(num_df[col], errors='coerce')
            processed_num_data = pipeline_num.transform(num_df)
        else:
            processed_num_data = pd.DataFrame()
        del num_df

        # Process Categorical attribute
        if cat_df.shape[0] != 0:
            pipeline_cat = PreprocessorCat(spec_cat, update_spec=False)
            processed_cat_data = pipeline_cat.transform(cat_df)
        else:
            processed_cat_data = pd.DataFrame()
        del cat_df

        # append ids with all variables
        output_data = pd.concat(
            (
                ids.reset_index(drop=True),
                processed_num_data.reset_index(drop=True),
                processed_cat_data.reset_index(drop=True),
            ),
            axis=1,
        )
        output_data[output_name] = loaded_model.predict(output_data)
        
        return output_data
    
    # 1. Retrieve data ========
    ids = df[['crn', 'ref_dt', 'Campaign_Seg_Grp_Id', 'template_id']]
                    
    # 2. Predict ========
    output_data = _predict(df, model, ids, output_name)
    return output_data[['crn', 'ref_dt', 'Campaign_Seg_Grp_Id', 'template_id', output_name]]


# adhoc only for proxy work
def run(part, 
        holdout_date = '', 
        prefix = '', 
        model_paths = {}
):
    print("Loading all artifacts...")
    
    client = storage.Client()
    bq = bigquery.Client()
    
    # Get all paths
    data_paths = []
    for blob in client.list_blobs('wx-personal', prefix=prefix+'/2_cmd_outputs/v01'):
        data_paths.append('gs://wx-personal/'+blob.name)
    data_paths = data_paths[(part-1)*10:part*10]

    # Get necessary CMD features
    MODELS = {k: load_model(model_paths[k]) for k in model_paths.keys()}
    model_columns = ['crn']
    for k in MODELS.keys():
        for col in MODELS[k].columns:
            if col.startswith('f0_'):
                if col.startswith('f0_b_'):
                    pass
                elif 'avg18wk_per_basket' in col:
                    pass
                elif 'p_rdm' in col:
                    pass
                else:
                    model_columns.append(col[3:])
            else:
                model_columns.append(col)
    model_columns = list(set(model_columns))

    # SQL to merge all features
    sql = f"""
        SELECT  
            audience.crn,
            audience.ref_dt,
            audience.Campaign_Seg_Grp_Id,
            audience.template_id,
            temp.* EXCEPT(crn),
            avg18wk.avg18wk_per_basket,
            mce.* EXCEPT(Campaign_Seg_Grp_Id, ref_dt),
            audience.redemption AS p_rdm,
        FROM `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_HOLDOUT_{holdout_date.replace('-','')}` audience

            JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe._TEMP_MMM_CAT_FS_OUTPUT_{part}` temp
                ON audience.crn = temp.crn

            -- last 18wk spend
            LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_avg18wk_per_basket_202204` AS avg18wk
                ON audience.crn = avg18wk.crn
                AND audience.ref_dt = avg18wk.ref_dt
                AND audience.campaign_seg_grp_id = avg18wk.campaign_seg_grp_id

            -- category embeddings
            LEFT JOIN `gcp-wow-rwds-ai-mmm-prod.dhioe.MMM_CAT_category_embeddings_202204` AS mce
                ON audience.Campaign_Seg_Grp_Id = mce.Campaign_Seg_Grp_Id
                AND audience.ref_dt = mce.ref_dt

    """
    
    print("Predicting...")
    
    for path in tqdm.tqdm(data_paths):
        
        # load CMD output and use bigquery to merge all the features
        temp_df = pd.read_parquet(path)
        missing_columns = list(set(model_columns) - set(temp_df.columns))
        for col in missing_columns:
            temp_df[col] = None
        temp_df = temp_df[model_columns]
        temp_df.to_gbq(
            f"gcp-wow-rwds-ai-mmm-prod.dhioe._TEMP_MMM_CAT_FS_OUTPUT_{part}", 
            if_exists='replace',
            progress_bar=False
        )

        # Update table expiry
        destination_table = bq.get_table(f"gcp-wow-rwds-ai-mmm-prod.dhioe._TEMP_MMM_CAT_FS_OUTPUT_{part}")
        destination_table.expires = datetime.datetime.now() + datetime.timedelta(hours=1)
        bq.update_table(destination_table, ['expires'])

        df = bq.query(sql).result().to_dataframe()
        
        # predict
        responses = []
        for proxy, model in MODELS.items():
            responses.append(adhoc_predict(model, df, proxy))

        # combine predictions
        response = pd.concat([responses[0], responses[1]['proxy_b'], df['p_rdm']], axis=1)
        response['proxy_c'] = response['proxy_b'] + 0.001 * response['p_rdm']
        
        response = response.sort_values(['crn', 'template_id']).reset_index(drop=True)
        
        # save to parquet
        response.to_parquet(
            f"gs://wx-personal/{prefix}/3_scoring_outputs/v01/{path.split('/')[-1]}",
            index=False,
        )