How to:

0. Prepare the data 
1. The following scripts can be run in parallel:
    - `retro.ipynb` to get predictions from the past
    - `ranking_prep.ipynb` to get `ranking_config` and `ranking_no_metric`
2. Run `proxy_{v}.ipynb` to get the model
3. Run `scoring_cmd_{v}.ipynb` to get the cmd features of the holdout set
4. Run `scoring_{i}` for i in 1:5 to score parallely
5. Run `post_scoring.ipynb` to copy results from 
6. Run `evaluation_all.ipynb` to evaluate