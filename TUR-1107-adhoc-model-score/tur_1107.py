import sys, os
import time
from datetime import datetime, timedelta
import pandas as pd

DATA_PATH = '/home/jovyan/repos/turing/TUR-1107-adhoc-model-score/data'

# partition_dates = [
#     '2021-09-20','2021-09-06','2021-08-23','2021-08-09','2021-07-26','2021-07-12','2021-06-28','2021-06-14',
#     '2021-05-31','2021-05-17','2021-05-03','2021-04-19','2021-04-05','2021-03-22','2021-03-08','2021-02-22',
#     '2021-01-25','2021-01-11','2020-12-28','2020-12-14','2020-11-30','2020-11-16','2020-11-02',
#     '2020-10-19','2020-10-05','2020-09-21','2020-09-07','2020-08-24'
# ]

partition_dates = [
    '2021-10-18', '2021-10-04','2021-09-20','2021-09-06','2021-08-23','2021-08-09','2021-07-26','2021-07-12',
]

columns = ['crn','campaign_type_long','template_id','p_rdm','base_spd','target_spd','target_cost']

# Copying data from GCS
# for partition_date in partition_dates:
#     print(f"Copying data from GCS into {DATA_PATH}/{partition_date}/...")
#     os.makedirs(f"{DATA_PATH}/{partition_date}/ranking_yes", exist_ok=True) 
#     os.system(f'gsutil -m -q cp -r gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/prod/{partition_date}/predicted_selected/* ./data/{partition_date}/ranking_yes/')
    
#     os.makedirs(f"{DATA_PATH}/{partition_date}/ranking_no", exist_ok=True) 
#     os.system(f'gsutil -m -q cp -r gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/prod/{partition_date}/ranking_no/* ./data/{partition_date}/ranking_no/')
    

# Process local files
for partition_date in partition_dates:
    stime = time.time()
    print(f"Processing {partition_date}...", end="")
    filelist = os.listdir(f"{DATA_PATH}/{partition_date}/ranking_yes/")
    dfs = [pd.read_parquet(f"{DATA_PATH}/{partition_date}/ranking_yes/{f}", columns=columns) for f in filelist]
    filelist = os.listdir(f"{DATA_PATH}/{partition_date}/ranking_no/")
    dfs += [pd.read_parquet(f"{DATA_PATH}/{partition_date}/ranking_no/{f}", columns=columns) for f in filelist]
    df = pd.concat(dfs, ignore_index=True)
    print(f"\tdf shape = {df.shape}", end="")
    df.rename(columns={'template_id':'Template_id'}, inplace=True)
    df['campaign_start_date'] = str(datetime.strptime(partition_date, "%Y-%m-%d") + timedelta(days=7))
    
    df.to_parquet(f'gs://wx-57a8a73b-9e03-4f1d-9349-bf67bf6d9c2c/data/dev/2021-10-11-tur-1107/{partition_date}/incoming/pred_selected_model_scores.parquet')
    print(f"\tTime taken = {time.time()-stime} sec.")