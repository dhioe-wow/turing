import os
import sys

from google.cloud import storage
import matplotlib.pyplot as plt
from matplotlib import gridspec
import numpy as np
import pandas as pd
import pickle

# import akl
from akl.preprocessor.categorical import PreprocessorCat
from akl.preprocessor.numeric import PreprocessorNum

import akl.lgbmfit
sys.modules['lgbmfit'] =  akl.lgbmfit

from sklearn.metrics import mean_squared_error, roc_auc_score


def load_model(gs_path_name):

    def _download(project_name, bucket_name, path_name):
        client = storage.Client(project_name)
        bucket = client.get_bucket(bucket_name)
        blob = bucket.blob(path_name)
        blob.download_to_filename('downloaded_model.pickle')

    def _unpickle(file_path):
        try:
            with open(file_path, 'rb') as f:
                obj = pickle.load(f)
                return obj
        except Exception as e:
            raise

    project_name = 'gcp-wow-rwds-ai-mmm-prod'
    bucket_name = gs_path_name[5:][:gs_path_name[5:].find('/')]
    path_name = gs_path_name[5:][gs_path_name[5:].find('/')+1:]

    _download(project_name, bucket_name, path_name)
    response = _unpickle('downloaded_model.pickle')
    
    return response


def adhoc_predict(model, data_path, score_type, use_mapping=True, legacy_columns=[]):
    
    def _cmd_map(x):
        if x[:3] == 'f0_':
            x = x[3:]
        x = x.replace('/', '_')
        if 'f03_scn_trn_' in x:
            return x
        else:
            return x.replace('_scn_','_')
        
    def _predict(df, loaded_model, ids, score_type):
        '''Load and prepare data based on score type
        Args:
            score_type (str): {p_rdm, base_spd, target_spd}
        '''

        def _get_feature_list(data, model_feature_list, model_obj):
            '''Get numeric and categorical features'''
            # get schema
            feature_list = data.columns.tolist()

            # get spec for both bum and cat
            spec_num = model_obj.config['spec_num']
            spec_cat = model_obj.config['spec_cat']

            cond = (spec_num.input != 'ignore') & (
                spec_num.new_feature.isin(model_feature_list)
            )
            spec_num_features = spec_num[cond].feature

            cond = (spec_cat.input != 'ignore') & (
                spec_cat.new_feature.isin(model_feature_list)
            )
            spec_cat_features = spec_cat[cond].feature

            num_list = [item for item in spec_num_features if item in feature_list]
            cat_list = [item for item in spec_cat_features if item in feature_list]

            return num_list, cat_list

        # get spec for both bum and cat
        spec_num = loaded_model.config['spec_num']
        spec_cat = loaded_model.config['spec_cat']

        # get list of model features
        # .values # lgbm==2.3.1 changed API from series to list
        model_feature_list = loaded_model.columns
        model_feature_list = pd.DataFrame(model_feature_list, dtype=str)[
            0
        ].apply(lambda x: x.split('||')[0])


        num_list, cat_list = _get_feature_list(
            df, model_feature_list, loaded_model
        )

        # separate in numerical and categorical
        num_df = df[num_list]
        cat_df = df[cat_list]
        # Process Numeric Attribute

        if num_df.shape[0] != 0:
            pipeline_num = PreprocessorNum(spec_num, update_spec=False)
            for col in num_df.columns:
                num_df[col] = pd.to_numeric(num_df[col], errors='coerce')
            processed_num_data = pipeline_num.transform(num_df)
        else:
            processed_num_data = pd.DataFrame()
        del num_df

        # Process Categorical attribute
        if cat_df.shape[0] != 0:
            pipeline_cat = PreprocessorCat(spec_cat, update_spec=False)
            processed_cat_data = pipeline_cat.transform(cat_df)
        else:
            processed_cat_data = pd.DataFrame()
        del cat_df

        # append ids with all variables
        output_data = pd.concat(
            (
                ids.reset_index(drop=True),
                processed_num_data.reset_index(drop=True),
                processed_cat_data.reset_index(drop=True),
            ),
            axis=1,
        )
        output_data[score_type] = loaded_model.predict(output_data)
        
        return output_data
    
    # 1. Retrieve data ========
    df = pd.read_parquet(data_path)
    
    if score_type == 'p_rdm':
        target = 'redeem_flag'
    else:
        target = 'as_wkly_spd_avg'
    ids = df[['crn', 'ref_dt', target]]
    
    # 2. Feature mapping ========
    if use_mapping:
        cmd_mapping = {x: _cmd_map(x) for x in model.columns}
        reverse_cmd_mapping = {v: k[3:] if k.startswith('f0_') else k for k, v in cmd_mapping.items()}

        check_columns = [v for k, v in cmd_mapping.items()]
        leftover_columns = [col for col in check_columns if col not in df.columns]
        
        # check if leftover columns are acceptable
        if len(leftover_columns) > 0:
            for leftover_column in leftover_columns:
                if leftover_column not in legacy_columns:
                    test = df[leftover_column] # raise error!
        
        df.rename(columns=reverse_cmd_mapping, inplace=True)
        for col in leftover_columns:
            df[col] = np.nan
                    
    # 3. Predict ========
    output_data = _predict(df, model, ids, score_type)
    return output_data[['crn', 'ref_dt', score_type, target]]


def calplot(df, model, columns, title='Calibration plot', bins=20):
    '''Draw calibration plot
    
    Args:
    - model [str]: {spd | rdm}
    - columns [list]: column names [pred_column_name, target_column_name]
    - title [str]: chart title
    - bins [int]: number of bins for the plot
    '''
    
    pred = columns[0]
    target = columns[1]
    
    _df = df[[pred, target]].sort_values(by=pred)
    _df = pd.cut(_df[pred], bins)
    _df = pd.DataFrame({'x': _df.apply(lambda x: x.mid)})
    _df = df.merge(_df, left_index=True, right_index=True)
    _df = _df.groupby(by='x').agg({target: 'mean', pred: 'mean'}).reset_index()
    _df = _df.dropna(how='any')
        
    if model == 'spd':
        limit = max(_df[pred].max(), _df[target].max())
        step = 10
    elif model == 'rdm':
        limit = 1.0
        step = 0.01
        
    fig = plt.figure(figsize=(10,10))
    spec = gridspec.GridSpec(
        ncols=1, nrows=2, height_ratios=[3, 1]
    )
    
    ax0 = fig.add_subplot(spec[0])
    ax0.set_title(title)
    ax0.grid()
    ax0.plot(np.arange(0,limit,step), np.arange(0,limit,step), '--', color='k')
    ax0.plot(_df[pred], _df[target])
    ax0.set_xlim(0, limit)
    ax0.set_ylim(0, limit)
    ax0.set_ylabel('true')

    ax1 = fig.add_subplot(spec[1], sharex = ax0)
    ax1.hist(df[pred], bins=20, edgecolor='w')
    ax1.grid()
    ax1.set_yscale('log')
    ax1.set_xlabel(pred)
    ax1.set_ylabel('volume')

    plt.show()
    
    
def liftplot(old_df, new_df, columns):
    
    pred = columns[0]
    target = columns[1]

    def get_lift_data(df, pred, target):
        response = df.sort_values(pred, ascending=False).reset_index(drop=True)
        response['p'] = response.index / max(response.index)
        decile = [*range(1,11)]

        def set_decile(x):
            for d in decile:
                if 10*x <= d:
                    return d

        response['decile'] = response['p'].apply(lambda x: set_decile(x))
        response = response[[target,'decile']].groupby('decile').agg('sum').reset_index()
        response[target] = response[target]/sum(response[target]/10)
        return response

    lift_df = get_lift_data(old_df, pred, target).merge(
        get_lift_data(new_df, pred, target), 
        left_on='decile', 
        right_on='decile'
    )

    plt.figure(figsize=(12, 5))
    plt.title('Lift chart comparison')
    plt.grid()
    plt.plot(lift_df['decile'], lift_df[f'{target}_x'], color = 'grey', label='old')
    plt.plot(lift_df['decile'], lift_df[f'{target}_y'], color = 'black', label='new')
    plt.legend()
    plt.show()
    
    
def compare(model, old_df, new_df):
    
    if model == 'rdm':
        old_roc_auc = roc_auc_score(old_df.redeem_flag, old_df.p_rdm)
        new_roc_auc = roc_auc_score(new_df.redeem_flag, new_df.p_rdm)

        print(f'ROC AUC (Old model): {old_roc_auc:.3f}')
        print(f'ROC AUC (New model): {new_roc_auc:.3f}')
        
        print('\nComparative lift chart ====')
        liftplot(old_df, new_df, ['p_rdm', 'redeem_flag'])
        
        print('\nCalibration plot ====')
        calplot(old_df, 'rdm', ['p_rdm', 'redeem_flag'], title='Calibration plot (Old redemption model)')
        calplot(new_df, 'rdm', ['p_rdm', 'redeem_flag'], title='Calibration plot (New redemption model)')
        
    if model == 'spd':
        old_rmse = np.sqrt(mean_squared_error(old_df.as_wkly_spd_avg, old_df.target_spd))
        new_rmse = np.sqrt(mean_squared_error(new_df.as_wkly_spd_avg, new_df.target_spd))

        print(f'RMSE (Old model): {old_rmse:.1f}')
        print(f'RMSE (New model): {new_rmse:.1f}')
        
        print('\nCalibration plot ====')
        calplot(old_df, 'spd', ['target_spd', 'as_wkly_spd_avg'], title='Calibration plot (Old spend model)')
        calplot(new_df, 'spd', ['target_spd', 'as_wkly_spd_avg'], title='Calibration plot (New spend model)')