DECLARE HOLDOUT_START_DATE DATE;
SET HOLDOUT_START_DATE = '2021-12-12';

DROP TABLE IF EXISTS `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_SAMPLED_202202`;
CREATE TABLE `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_SAMPLED_202202` AS (
    SELECT * EXCEPT (seqnum)
    FROM (
        SELECT *, ROW_NUMBER() OVER (PARTITION BY campaign_seg_grp_id ORDER BY RAND()) as seqnum
        FROM `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_202202`
        WHERE Model ='R'
          AND ref_dt < HOLDOUT_START_DATE
    ) foo
    WHERE seqnum <= 100000
);

DROP TABLE IF EXISTS `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_HOLDOUT_202202`;
CREATE TABLE `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_HOLDOUT_202202` AS (
    SELECT * EXCEPT (seqnum)
    FROM (
      SELECT *, ROW_NUMBER() OVER (ORDER BY RAND()) as seqnum
      FROM  `gcp-wow-rwds-ai-mmm-prod.dev_category_offer.MMM_CAT_FINAL_FEATURES_CAMP_202202`
      WHERE ref_dt >= HOLDOUT_START_DATE
    ) foo
    WHERE seqnum <= 1000000
);