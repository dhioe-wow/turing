To reproduce, use the following steps:

1. Run embedding.ipynb to create `gcp-wow-rwds-ai-mmm-super-prod.dev_category_offer.MMM_CAT_category_embeddings_{YYYYMM}` table
    - Need to get all past execution dates
    
2. Run `decision-engine/mmm_super_cat/schedule/model_training/sql_rendered/all_cust_features.sql` to create `gcp-wow-rwds-ai-mmm-super-prod.dev_category_offer.MMM_FINAL_FEATURES_CAMP_{YYYYMM}` 
    - Suggest to use `PROD_MMM_CAT` dataset for all input tables 
    - Suggest to use `dev_category_offer` dataset for all temporary and final output tables
    - Suggest to add `_{YYYYMM}` suffix to all temporary and final output tables
    
3. Run `sampling.sql` to create `gcp-wow-rwds-ai-mmm-super-prod.dev_category_offer.MMM_FINAL_FEATURES_CAMP_SAMPLED_{YYYYMM}`
    - Make sure to exclude holdout set from this sampled dataset --> the latter is used for training and validation
    - Suggest to use the entire unsampled holdout dataset as evaluation dataset
    
4. Run `rdm_{v}.ipynb` and `spd_{v}.ipynb` to train the model
    - Suggest to cleanup GCS folders first if wish to repeat experiment
    
5. Run `scoring_cmd_{v}.ipynb` to get feature storm features for the holdout set

6. Run `scoring.ipynb` to evaluate old and new models on the holdout set
    - Make sure all features from past is available in the new datasets
    
7. Run `post_analysis.ipynb` to check the results